﻿

using System;
using FastColoredTextBoxNS;

namespace Guinea
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Натуральные числа с нулем");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Целые числа");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Рациональные числа");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Многочлен с рациональными коэффициентами");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ModulesTree = new System.Windows.Forms.TreeView();
            this.menu = new System.Windows.Forms.ToolStrip();
            this.HelpMenuButton = new System.Windows.Forms.ToolStripButton();
            this.SettingsButton = new System.Windows.Forms.ToolStripButton();
            this.RepositoryButton = new System.Windows.Forms.ToolStripButton();
            this.BugReportButton = new System.Windows.Forms.ToolStripButton();
            this.Console = new Guinea.FastConsole();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Console)).BeginInit();
            this.SuspendLayout();
            // 
            // ModulesTree
            // 
            this.ModulesTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ModulesTree.Location = new System.Drawing.Point(16, 34);
            this.ModulesTree.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModulesTree.MinimumSize = new System.Drawing.Size(625, 633);
            this.ModulesTree.Name = "ModulesTree";
            treeNode1.Name = "Natural";
            treeNode1.Tag = "ParentModule";
            treeNode1.Text = "Натуральные числа с нулем";
            treeNode2.Name = "Integer";
            treeNode2.Tag = "ParentModule";
            treeNode2.Text = "Целые числа";
            treeNode3.Name = "Rational";
            treeNode3.Tag = "ParentModule";
            treeNode3.Text = "Рациональные числа";
            treeNode4.Name = "Polynomial";
            treeNode4.Tag = "ParentModule";
            treeNode4.Text = "Многочлен с рациональными коэффициентами";
            this.ModulesTree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4});
            this.ModulesTree.Size = new System.Drawing.Size(625, 633);
            this.ModulesTree.TabIndex = 1;
            this.ModulesTree.Tag = "text";
            this.ModulesTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.ModulesTree_NodeMouseClick);
            this.ModulesTree.Enter += new System.EventHandler(this.ModulesTree_Enter);
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.Color.Transparent;
            this.menu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menu.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menu.GripMargin = new System.Windows.Forms.Padding(0);
            this.menu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.menu.ImageScalingSize = new System.Drawing.Size(0, 0);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HelpMenuButton,
            this.SettingsButton,
            this.RepositoryButton,
            this.BugReportButton});
            this.menu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menu.Size = new System.Drawing.Size(1232, 27);
            this.menu.TabIndex = 7;
            this.menu.Text = "Меню";
            // 
            // HelpMenuButton
            // 
            this.HelpMenuButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.HelpMenuButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HelpMenuButton.Name = "HelpMenuButton";
            this.HelpMenuButton.Size = new System.Drawing.Size(67, 24);
            this.HelpMenuButton.Tag = "menu";
            this.HelpMenuButton.Text = "Помощь";
            this.HelpMenuButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // SettingsButton
            // 
            this.SettingsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SettingsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(94, 24);
            this.SettingsButton.Tag = "menu";
            this.SettingsButton.Text = "Настройки";
            this.SettingsButton.Click += new System.EventHandler(this.SettingsButton_Click);
            // 
            // RepositoryButton
            // 
            this.RepositoryButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.RepositoryButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RepositoryButton.Name = "RepositoryButton";
            this.RepositoryButton.Size = new System.Drawing.Size(112, 24);
            this.RepositoryButton.Tag = "menu";
            this.RepositoryButton.Text = "Репозиторий";
            this.RepositoryButton.Click += new System.EventHandler(this.RepositoryButton_Click);
            // 
            // BugReportButton
            // 
            this.BugReportButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.BugReportButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BugReportButton.Name = "BugReportButton";
            this.BugReportButton.Size = new System.Drawing.Size(139, 24);
            this.BugReportButton.Tag = "menu";
            this.BugReportButton.Text = "Обратная связь";
            this.BugReportButton.Click += new System.EventHandler(this.BugReportButton_Click);
            // 
            // Console
            // 
            this.Console.AllowMacroRecording = false;
            this.Console.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Console.AutoCompleteBrackets = true;
            this.Console.AutoCompleteBracketsList = new char[] {
        '(',
        ')'};
            this.Console.AutoIndent = false;
            this.Console.AutoIndentChars = false;
            this.Console.AutoIndentExistingLines = false;
            this.Console.AutoScrollMinSize = new System.Drawing.Size(2, 22);
            this.Console.BackBrush = null;
            this.Console.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Console.CaretColor = System.Drawing.Color.White;
            this.Console.CharHeight = 22;
            this.Console.CharWidth = 10;
            this.Console.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Console.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.Console.Font = new System.Drawing.Font("Consolas", 10.8F);
            this.Console.Hotkeys = resources.GetString("Console.Hotkeys");
            this.Console.IsReplaceMode = false;
            this.Console.LineInterval = 1;
            this.Console.Location = new System.Drawing.Point(645, 34);
            this.Console.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Console.Name = "Console";
            this.Console.Paddings = new System.Windows.Forms.Padding(0);
            this.Console.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Console.ServiceColors = null;
            this.Console.ShowLineNumbers = false;
            this.Console.Size = new System.Drawing.Size(570, 633);
            this.Console.TabIndex = 9;
            this.Console.Tag = "inout";
            this.Console.Zoom = 100;
            this.Console.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Console.ToolTipNeeded += new System.EventHandler<FastColoredTextBoxNS.ToolTipNeededEventArgs>(this.Console_ToolTipNeeded);
            this.Console.TextChanged += new System.EventHandler<FastColoredTextBoxNS.TextChangedEventArgs>(this.Console_TextChanged);
            this.Console.SelectionChanged += new System.EventHandler(this.Console_SelectionChanged);
            this.Console.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Console_KeyDown);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1232, 683);
            this.Controls.Add(this.Console);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.ModulesTree);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(1226, 667);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "menu";
            this.Text = "Guinea";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Console)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private FastConsole Console;
        private System.Windows.Forms.TreeView ModulesTree;
        private System.Windows.Forms.ToolStrip menu;
        private System.Windows.Forms.ToolStripButton HelpMenuButton;
        private System.Windows.Forms.ToolStripButton SettingsButton;
        private System.Windows.Forms.ToolStripButton RepositoryButton;
        private System.Windows.Forms.ToolStripButton BugReportButton;
    }
}

