﻿using Guinea.Environment;
using Guinea.GUI;
using Guinea.GUI.Forms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Guinea
{
    public partial class SettingsForm : Form
    {
        private Boolean ready = false;
        public SettingsForm( ) //pass config into constructor!
        {
            InitializeComponent();
            
            this.processingTheme = new Theme(Theme.currentTheme);
        }
        
        
        public Theme processingTheme { get; set; }

        private int currThemeIndex = 0;

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            
            foreach(Theme theme in Theme.themes)
            {
                listBox1.Items.Add(theme.name);
            }
            currThemeIndex = listBox1.Items.IndexOf(Theme.currentTheme.name);
            
            checkBox1.Checked = Config.cfg.showOnlyRequested;

            ready = true;
            listBox1.Focus();
            listBox1.SelectedIndex = currThemeIndex;
        }

        private void updateThemeList()
        {
            listBox1.BeginUpdate();
            listBox1.Items.Clear();
            foreach (Theme theme in Theme.themes)
            {
                listBox1.Items.Add(theme.name);
            }
            listBox1.EndUpdate();
        }


        private void SettingsSaveButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0 || listBox1.SelectedItem == null)
            {
                this.DialogResult = DialogResult.Cancel;
                return;
            }
            Theme.currentTheme = Theme.themes.Find(x => x.name == listBox1.SelectedItem.ToString());
            saveThemes();
            Config.cfg.save();
            this.DialogResult = DialogResult.OK;
        }

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();

            Font res = ( e.Index == currThemeIndex )?(new Font(e.Font, FontStyle.Bold)):e.Font;

            e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), res , Brushes.Black, e.Bounds);
            e.DrawFocusRectangle();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0 || listBox1.SelectedIndex >= listBox1.Items.Count)
            {
                return;
            }

            this.processingTheme = Theme.themes.Find(x => x.name == listBox1.Items[listBox1.SelectedIndex].ToString());
            if (listBox1.SelectedIndex < 2)
            {
                removeThemeButton.Enabled = false;
                editThemeButton.Enabled = false;
            }
            else
            {
                removeThemeButton.Enabled = true;
                editThemeButton.Enabled = true;
            }


        }

        private void copyThemeButton_Click(object sender, EventArgs e)
        {
            Theme newTheme = new Theme(processingTheme);
            while (listBox1.Items.Contains(newTheme.name))
            {
                newTheme.name = processingTheme.name + " - копия";
            }


            Theme.themes.Add(newTheme);
            updateThemeList();
            listBox1.SelectedIndex = listBox1.Items.IndexOf(newTheme.name);
        }

        private void removeThemeButton_Click(object sender, EventArgs e)
        {
            int previndex = listBox1.SelectedIndex;
            Theme.themes.Remove(Theme.themes.Find(x => x.name == listBox1.Items[listBox1.SelectedIndex].ToString()));

            String name = listBox1.Items[listBox1.SelectedIndex].ToString();
            String path = Config.path + Path.DirectorySeparatorChar + "Themes";
            if (File.Exists(path + Path.DirectorySeparatorChar + name + ".json"))
            {
                File.Delete(path + Path.DirectorySeparatorChar + name + ".json");
            }


            updateThemeList();
            listBox1.SelectedIndex = previndex-1;

        }

        private void editThemeButton_Click(object sender, EventArgs e)
        {
            List<String> names = new List<String>();
            foreach(Theme th in Theme.themes)
            {
                if(th == this.processingTheme)
                {
                    continue;
                }
                names.Add(th.name);
            }

            EditThemeForm edit = new EditThemeForm(new Theme(this.processingTheme), names);
            edit.ShowDialog();
            if (edit.DialogResult == DialogResult.OK)
            {
                int index = Theme.themes.IndexOf(this.processingTheme);
                Theme.themes.RemoveAt(index);
                Theme.themes.Insert(index, edit.resultTheme);
                updateThemeList();
            }
        }

        private void saveThemes()
        {
            String path = Config.path + Path.DirectorySeparatorChar + "Themes";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            foreach (Theme th in Theme.themes)
            {
                if(th.name == "Default Light theme" || th.name == "Default Dark theme")
                {
                    continue;
                }
                if (File.Exists(path + Path.DirectorySeparatorChar + th.name+".json"))
                {
                    File.Delete(path + Path.DirectorySeparatorChar + th.name + ".json");
                }
                File.WriteAllText(path + Path.DirectorySeparatorChar + th.name + ".json", JsonConvert.SerializeObject(th, Formatting.Indented));
            }
        }
        


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            /** МИША И ТРИГГЕРЫ
             * 
             * 
             * 
             * */
        }
    }
}
