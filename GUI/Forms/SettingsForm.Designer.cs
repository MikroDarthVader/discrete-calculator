﻿
namespace Guinea
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SettingsSaveButton = new System.Windows.Forms.Button();
            this.SettingsCancelButton = new System.Windows.Forms.Button();
            this.SettingsResetButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.copyThemeButton = new System.Windows.Forms.Button();
            this.removeThemeButton = new System.Windows.Forms.Button();
            this.editThemeButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // SettingsSaveButton
            // 
            this.SettingsSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsSaveButton.AutoSize = true;
            this.SettingsSaveButton.Location = new System.Drawing.Point(320, 0);
            this.SettingsSaveButton.Margin = new System.Windows.Forms.Padding(0);
            this.SettingsSaveButton.Name = "SettingsSaveButton";
            this.SettingsSaveButton.Size = new System.Drawing.Size(100, 23);
            this.SettingsSaveButton.TabIndex = 9;
            this.SettingsSaveButton.Text = "Сохранить";
            this.SettingsSaveButton.UseVisualStyleBackColor = true;
            this.SettingsSaveButton.Click += new System.EventHandler(this.SettingsSaveButton_Click);
            // 
            // SettingsCancelButton
            // 
            this.SettingsCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsCancelButton.AutoSize = true;
            this.SettingsCancelButton.Location = new System.Drawing.Point(225, 0);
            this.SettingsCancelButton.Margin = new System.Windows.Forms.Padding(0);
            this.SettingsCancelButton.Name = "SettingsCancelButton";
            this.SettingsCancelButton.Size = new System.Drawing.Size(75, 23);
            this.SettingsCancelButton.TabIndex = 10;
            this.SettingsCancelButton.Text = "Отмена";
            this.SettingsCancelButton.UseVisualStyleBackColor = true;
            // 
            // SettingsResetButton
            // 
            this.SettingsResetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SettingsResetButton.AutoSize = true;
            this.SettingsResetButton.Location = new System.Drawing.Point(0, 0);
            this.SettingsResetButton.Margin = new System.Windows.Forms.Padding(0);
            this.SettingsResetButton.Name = "SettingsResetButton";
            this.SettingsResetButton.Size = new System.Drawing.Size(75, 23);
            this.SettingsResetButton.TabIndex = 11;
            this.SettingsResetButton.Text = "Сбросить";
            this.SettingsResetButton.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.SettingsSaveButton, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.SettingsResetButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.SettingsCancelButton, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 386);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(420, 22);
            this.tableLayoutPanel2.TabIndex = 13;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.listBox1.IntegralHeight = false;
            this.listBox1.Location = new System.Drawing.Point(12, 55);
            this.listBox1.Name = "listBox1";
            this.listBox1.ScrollAlwaysVisible = true;
            this.listBox1.Size = new System.Drawing.Size(300, 325);
            this.listBox1.TabIndex = 14;
            this.listBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBox1_DrawItem);
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // copyThemeButton
            // 
            this.copyThemeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.copyThemeButton.Location = new System.Drawing.Point(0, 104);
            this.copyThemeButton.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.copyThemeButton.Name = "copyThemeButton";
            this.copyThemeButton.Size = new System.Drawing.Size(100, 33);
            this.copyThemeButton.TabIndex = 15;
            this.copyThemeButton.Text = "Копировать";
            this.copyThemeButton.UseVisualStyleBackColor = true;
            this.copyThemeButton.Click += new System.EventHandler(this.copyThemeButton_Click);
            // 
            // removeThemeButton
            // 
            this.removeThemeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.removeThemeButton.Enabled = false;
            this.removeThemeButton.Location = new System.Drawing.Point(0, 145);
            this.removeThemeButton.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.removeThemeButton.Name = "removeThemeButton";
            this.removeThemeButton.Size = new System.Drawing.Size(100, 33);
            this.removeThemeButton.TabIndex = 16;
            this.removeThemeButton.Text = "Удалить";
            this.removeThemeButton.UseVisualStyleBackColor = true;
            this.removeThemeButton.Click += new System.EventHandler(this.removeThemeButton_Click);
            // 
            // editThemeButton
            // 
            this.editThemeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editThemeButton.Enabled = false;
            this.editThemeButton.Location = new System.Drawing.Point(0, 186);
            this.editThemeButton.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.editThemeButton.Name = "editThemeButton";
            this.editThemeButton.Size = new System.Drawing.Size(100, 35);
            this.editThemeButton.TabIndex = 17;
            this.editThemeButton.Text = "Изменить";
            this.editThemeButton.UseVisualStyleBackColor = true;
            this.editThemeButton.Click += new System.EventHandler(this.editThemeButton_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.copyThemeButton, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.editThemeButton, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.removeThemeButton, 0, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(332, 55);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(100, 325);
            this.tableLayoutPanel3.TabIndex = 18;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(12, 12);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(420, 24);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "Показывать только необходимые для работы модули";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Темы";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 421);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.MinimumSize = new System.Drawing.Size(460, 460);
            this.Name = "SettingsForm";
            this.Tag = "menu";
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button SettingsSaveButton;
        private System.Windows.Forms.Button SettingsCancelButton;
        private System.Windows.Forms.Button SettingsResetButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button copyThemeButton;
        private System.Windows.Forms.Button removeThemeButton;
        private System.Windows.Forms.Button editThemeButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label1;
    }
}