﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Guinea.GUI;
using Guinea.Environment;
using FastColoredTextBoxNS;
using Newtonsoft.Json;

namespace Guinea
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            Theme.currentTheme = new Theme();
            menu.Renderer = new GUI.Forms.CustomControls.MySR();
            loadConfig();
            loadThemes();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Theme.currentTheme.ApplyTheme(this);
#if (!DEBUG)
            Text += " v:" + Assembly.GetEntryAssembly().GetName().Version;
#endif

            FastConsole.ConsoleInputAPI.Console = Console;
            FillModulesTree();
            ActiveControl = Console;
            Console.OnLoad();
        }

        private void ModulesTree_Enter(object sender, EventArgs e)
        {
            Console.Focus();
            ActiveControl = Console;
        }

        private void FillModulesTree()
        {
            foreach (IModule module in UI.Modules.Values)
            {
                if (module.VisibleInTree)
                {
                    switch (module.Type)
                    {
                        case moduleType.natural:
                            ModulesTree.Nodes["Natural"].Nodes.Add(module.PrintableName);
                            break;

                        case moduleType.integer:
                            ModulesTree.Nodes["Integer"].Nodes.Add(module.PrintableName);
                            break;

                        case moduleType.rational:
                            ModulesTree.Nodes["Rational"].Nodes.Add(module.PrintableName);
                            break;

                        case moduleType.polynomial:
                            ModulesTree.Nodes["Polynomial"].Nodes.Add(module.PrintableName);
                            break;
                    }
                }
            }
        }

        private void SettingsButton_Click(object sender, EventArgs e)
        {
            SettingsForm settings = new SettingsForm();
            settings.ShowDialog();
            if (settings.DialogResult == DialogResult.OK)
            {
                Theme.currentTheme.ApplyTheme(this);
            }
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            ProcessStartInfo link = new ProcessStartInfo("https://bitbucket.org/MikroDarthVader/discrete-calculator/src/master/README.md");
            Process.Start(link);
            /*AboutForm help = new AboutForm();
            help.Show();*/
        }

        private void RepositoryButton_Click(object sender, EventArgs e)
        {
            ProcessStartInfo link = new ProcessStartInfo("https://bitbucket.org/MikroDarthVader/discrete-calculator/overview");
            Process.Start(link);
        }

        private void BugReportButton_Click(object sender, EventArgs e)
        {
            ProcessStartInfo link = new ProcessStartInfo("https://bitbucket.org/MikroDarthVader/discrete-calculator/issues");
            Process.Start(link);
        }

        private void ModulesTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag != null && e.Node.Tag.ToString().Contains("ParentModule"))
            {
                if (ModulesTree.HitTest(e.Location).Location != TreeViewHitTestLocations.PlusMinus)
                    e.Node.Toggle();
            }
            else
                Console.InsertModuleInConsole(UI.Modules.Values.ToList().Find(x => x.PrintableName == e.Node.Text));
        }


        private void Console_KeyDown(object sender, KeyEventArgs e)
        {
            Console.ShiftStateChange(e.Shift);

            if (e.KeyData == (Keys.Escape))
            {
                Console.StopCalculating();
            }

            if (e.KeyCode == Keys.Enter && !Console.popupOpened)
                Console.SelectionStart = Console.Text.Length;

            if (Console.Lines.Count != 0)
            {
                if (e.KeyData == (Keys.Up | Keys.Shift))
                {
                    Console.HistoryUp(true);
                }
                else if (e.KeyData == (Keys.Down | Keys.Shift))
                {
                    Console.HistoryDown(true);
                }
                else if (e.KeyData == Keys.Up)
                {
                    Console.HistoryUp(false);
                }
                else if (e.KeyData == Keys.Down)
                {
                    Console.HistoryDown(false);
                }
            }
        }

        private void Console_TextChanged(object sender, TextChangedEventArgs e)
        {
            Console.Console_TextChanged(sender, e);
        }

        private void Console_SelectionChanged(object sender, EventArgs e)
        {
            Console.ResetHistoryPos();
        }

        private bool isEqualsSeps(char symbol)
        {
            string seps = "=(;)";
            foreach (char sym in seps)
            {
                if (sym == symbol)
                    return true;
            }

            return false;
        }







        private void Console_ToolTipNeeded(object sender, ToolTipNeededEventArgs e)
        {
            Place tempStart = Console.Selection.Start;
            Place tempEnd = Console.Selection.End;

            Place start, end = e.Place;
            int argumentNumber = 0;
            try
            {
                for (end.iChar--; end.iChar > 0 && Console.Lines[end.iLine][end.iChar] != '('; end.iChar--)
                {
                    if (Console.Lines[end.iLine][end.iChar] == ';')
                    {
                        argumentNumber++;
                    }
                }
                if (end.iChar > 0)
                {
                    start = end;
                    for (start.iChar = end.iChar == 0 ? 0 : end.iChar - 1; start.iChar > 0 && !isEqualsSeps(Console.Lines[start.iLine][start.iChar]); start.iChar--) ;
                    Console.Selection.Start = start;
                    Console.Selection.End = end;

                    foreach (IModule module in UI.Modules.Values)
                    {
                        if (module.ToString().Substring("Guinea.Modules.".Length) == Console.SelectedText)
                        {
                            e.ToolTipText = module.Hints[argumentNumber];
                            break;
                        }
                    }
                }
            }
            finally
            {
                Console.Selection.Start = tempStart;
                Console.Selection.End = tempEnd;
            }
        }



        private void loadConfig()
        {
            if (File.Exists(Config.path + Path.DirectorySeparatorChar + "config.json"))
            {
                try
                {
                    Config.cfg = JsonConvert.DeserializeObject<Config>(File.ReadAllText(Config.path + Path.DirectorySeparatorChar + "config.json"));
                }
                catch (Exception ee)
                {
                    File.Delete(Config.path + Path.DirectorySeparatorChar + "config.json");
                    Config.cfg = new Config();
                }
            }
            else
            {
                Config.cfg = new Config();
            }
        }

        private Theme loadTheme(String path)
        {
            Theme th;
            try
            {
                th = JsonConvert.DeserializeObject<Theme>(File.ReadAllText(path));
            }
            catch (Exception ee)
            {
                File.Delete(path);
                th = null;
            }
            return th;
        }

        private void loadThemes()
        {
            Theme.themes.Clear();
            Theme.themes.Add(new Theme(0));
            Theme.themes.Add(new Theme(1));
            String path = Config.path + Path.DirectorySeparatorChar + "Themes";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            foreach (String file in Directory.GetFiles(path))
            {
                Theme th = loadTheme(file);
                String resultName = (file.Substring(file.LastIndexOf(Path.DirectorySeparatorChar))).Split('.')[0];
                if (th.name != resultName)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch { }
                }
                if (th != null)
                {
                    Theme.themes.Add(th);
                }

                
                Theme.currentTheme = Theme.themes.Find(x => x.name == Config.cfg.selectedTheme);
            }


        }



    }
}