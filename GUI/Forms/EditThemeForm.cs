﻿
using Guinea.GUI.Forms.CustomControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Guinea.GUI.Forms
{



    public partial class EditThemeForm : Form
    {
        /*
        private const int WM_SETCURSOR = 0x20;
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == WM_SETCURSOR) //WM_SETCURSOR is set to 0x20
                return;
        }
        */
        public EditThemeForm(Theme theme, List<String> names)
        {
            InitializeComponent();
            Theme.currentTheme.ApplyTheme(this);
            resultTheme = new Theme(theme);
            this.names = names;
        }

        private int lastIndex;
        private List<String> names;
        public Theme resultTheme;
        private Boolean ready = false;

        private void EditThemeForm_Load(object sender, EventArgs e)
        {
            VarSelection.SelectedIndex = 1;
            VarValListView.AllowUserToResizeRows = false;
            
            
            ready = true;
            lastIndex = 1;
            VarSelection.SelectedIndex = 0;
        }

        private void VarSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (!ready) { return; }
            if(VarSelection.SelectedIndex == 0 && lastIndex == 1)
            {
                lastIndex = 0;
                if (VarValListView.Rows.Count > 1)
                {
                    resultTheme.ModulesHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_ModulesHighlihter", true)[0]).BackColor;
                    resultTheme.VariablesHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_VariablesHighlihter", true)[0]).BackColor;
                    resultTheme.DatasHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_DatasHighlihter", true)[0]).BackColor;
                    resultTheme.SeparatorsHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_SeparatorsHighlihter", true)[0]).BackColor;
                    resultTheme.AnswerHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_AnswerHighlihter", true)[0]).BackColor;
                    resultTheme.AnswerErrorHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_AnswerErrorHighlihter", true)[0]).BackColor;
                    resultTheme.ExceptionWordHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_ExceptionWordHighlihter", true)[0]).BackColor;
                    resultTheme.ErrorHighlighter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_ErrorHighlighter", true)[0]).BackColor;

                    resultTheme.PopupMenuBackColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_PopupMenuBackColor", true)[0]).BackColor;
                    resultTheme.PopupMenuSelectionBackColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_PopupMenuSelectionBackColor", true)[0]).BackColor;
                    resultTheme.PopupMenuSelectionForeColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_PopupMenuSelectionForeColor", true)[0]).BackColor;



                    VarValListView.Controls.Clear();
                    VarValListView.Controls.Clear();
                    VarValListView.Controls.Clear(); //одного должно хватить, но оно просто не работает без этого :(
                    VarValListView.Controls.Clear();
                    VarValListView.Rows.Clear();
                    VarValListView.Update();
                }

                VarValListView.Rows.Add("Название", resultTheme.name);
                VarValListView.Rows[0].ReadOnly = false;
                VarValListView.Rows[0].Cells[0].ReadOnly = true;

                //https://stackoverflow.com/questions/25451604/programmatically-add-cells-and-rows-to-datagridview


                VarValListView.Rows.Add("Шрифт текста", resultTheme.textFont.FontFamily.Name + ", " + resultTheme.textFont.Size);
                VarValListView.Rows[1].Cells[1].ReadOnly = true;
                VarValListView.Rows[1].Cells[1].Style.Font = resultTheme.textFont;

                VarValListView.Rows.Add("Цвет текста", " ");
                CustomColourPicker textColor = new CustomColourPicker(resultTheme.textColor, "textColor");
                textColor.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel textColorCell = textColor.getControls();

                
                VarValListView.Rows.Add("Цвет фона текста", " ");
                CustomColourPicker textBackColor = new CustomColourPicker(resultTheme.textBackColor, "textBackColor");
                textBackColor.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel textBackColorCell = textBackColor.getControls();
                
                
                VarValListView.Rows.Add("Шрифт текста меню", resultTheme.menuFont.FontFamily.Name + ", " + resultTheme.menuFont.Size);
                VarValListView.Rows[4].Cells[1].ReadOnly = true;
                VarValListView.Rows[4].Cells[1].Style.Font = resultTheme.menuFont;

                VarValListView.Rows.Add("Цвет текста меню", " ");
                CustomColourPicker menuColor = new CustomColourPicker(resultTheme.menuColor, "menuColor");
                menuColor.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel menuColorCell = menuColor.getControls();


                VarValListView.Rows.Add("Цвет фона меню", " ");
                CustomColourPicker menuBackColor = new CustomColourPicker(resultTheme.menuBackColor, "menuBackColor");
                menuBackColor.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel menuBackColorCell = menuBackColor.getControls();



                this.VarValListView.Controls.Add(textColorCell);
                textColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 2, true).Location;
                textColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 2, true).Size;

                this.VarValListView.Controls.Add(textBackColorCell);
                textBackColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 3, true).Location;
                textBackColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 3, true).Size;
                
                this.VarValListView.Controls.Add(menuColorCell);
                menuColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 5, true).Location;
                menuColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 5, true).Size;

                this.VarValListView.Controls.Add(menuBackColorCell);
                menuBackColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 6, true).Location;
                menuBackColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 6, true).Size;


                foreach (DataGridViewRow row in VarValListView.Rows)
                {
                    row.Cells[0].ReadOnly = true;
                    row.Cells[1].ReadOnly = true;
                }
                VarValListView.Rows[0].Cells[1].ReadOnly = false;

            }
            else if (VarSelection.SelectedIndex == 1 && lastIndex == 0)
            {
                lastIndex = 1;
                if (VarValListView.Rows.Count > 1)
                {
                    resultTheme.name = (String)VarValListView.Rows[0].Cells[1].Value;
                    resultTheme.textColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_textColor", true)[0]).BackColor;
                    resultTheme.textBackColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_textBackColor", true)[0]).BackColor;
                    resultTheme.menuColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_menuColor", true)[0]).BackColor;
                    resultTheme.menuBackColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_menuBackColor", true)[0]).BackColor;

                    resultTheme.textFont = VarValListView.Rows[1].Cells[1].Style.Font;
                    resultTheme.menuFont = VarValListView.Rows[4].Cells[1].Style.Font;

                    VarValListView.Controls.Clear();
                    VarValListView.Controls.Clear();
                    VarValListView.Controls.Clear();
                    VarValListView.Controls.Clear();
                    VarValListView.Rows.Clear();
                    VarValListView.Update();
                }
                VarValListView.Rows.Add("ModulesHighlihter", " ");
                CustomColourPicker ModulesHighlihter = new CustomColourPicker(resultTheme.ModulesHighlihter, "ModulesHighlihter");
                ModulesHighlihter.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel ModulesHighlihterCell = ModulesHighlihter.getControls();

                VarValListView.Rows.Add("VariablesHighlihter", " ");
                CustomColourPicker VariablesHighlihter = new CustomColourPicker(resultTheme.VariablesHighlihter, "VariablesHighlihter");
                VariablesHighlihter.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel VariablesHighlihterCell = VariablesHighlihter.getControls();

                VarValListView.Rows.Add("DatasHighlihter", " ");
                CustomColourPicker DatasHighlihter = new CustomColourPicker(resultTheme.DatasHighlihter, "DatasHighlihter");
                DatasHighlihter.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel DatasHighlihterCell = DatasHighlihter.getControls();

                VarValListView.Rows.Add("SeparatorsHighlihter", " ");
                CustomColourPicker SeparatorsHighlihter = new CustomColourPicker(resultTheme.SeparatorsHighlihter, "SeparatorsHighlihter");
                SeparatorsHighlihter.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel SeparatorsHighlihterCell = SeparatorsHighlihter.getControls();

                VarValListView.Rows.Add("AnswerHighlihter", " ");
                CustomColourPicker AnswerHighlihter = new CustomColourPicker(resultTheme.AnswerHighlihter, "AnswerHighlihter");
                AnswerHighlihter.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel AnswerHighlihterCell = AnswerHighlihter.getControls();

                VarValListView.Rows.Add("AnswerErrorHighlihter", " ");
                CustomColourPicker AnswerErrorHighlihter = new CustomColourPicker(resultTheme.AnswerErrorHighlihter, "AnswerErrorHighlihter");
                AnswerErrorHighlihter.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel AnswerErrorHighlihterCell = AnswerErrorHighlihter.getControls();

                VarValListView.Rows.Add("ExceptionWordHighlihter", " ");
                CustomColourPicker ExceptionWordHighlihter = new CustomColourPicker(resultTheme.ExceptionWordHighlihter, "ExceptionWordHighlihter");
                ExceptionWordHighlihter.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel ExceptionWordHighlihterCell = ExceptionWordHighlihter.getControls();

                VarValListView.Rows.Add("ErrorHighlighter", " ");
                CustomColourPicker ErrorHighlighter = new CustomColourPicker(resultTheme.ErrorHighlighter, "ErrorHighlighter");
                ErrorHighlighter.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel ErrorHighlighterCell = ErrorHighlighter.getControls();


                VarValListView.Rows.Add("PopupMenuBackColor", " ");
                CustomColourPicker PopupMenuBackColor = new CustomColourPicker(resultTheme.PopupMenuBackColor, "PopupMenuBackColor");
                PopupMenuBackColor.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel PopupMenuBackColorCell = PopupMenuBackColor.getControls();

                VarValListView.Rows.Add("PopupMenuSelectionBackColor", " ");
                CustomColourPicker PopupMenuSelectionBackColor = new CustomColourPicker(resultTheme.PopupMenuSelectionBackColor, "PopupMenuSelectionBackColor");
                PopupMenuSelectionBackColor.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel PopupMenuSelectionBackColorCell = PopupMenuSelectionBackColor.getControls();

                VarValListView.Rows.Add("PopupMenuSelectionForeColor", " ");
                CustomColourPicker PopupMenuSelectionForeColor = new CustomColourPicker(resultTheme.PopupMenuSelectionForeColor, "PopupMenuSelectionForeColor");
                PopupMenuSelectionForeColor.addClickHandler(new EventHandler(VarValListView_Click));
                TableLayoutPanel PopupMenuSelectionForeColorCell = PopupMenuSelectionForeColor.getControls();



                this.VarValListView.Controls.Add(ModulesHighlihterCell);
                ModulesHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 0, true).Location;
                ModulesHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 0, true).Size;

                this.VarValListView.Controls.Add(VariablesHighlihterCell);
                VariablesHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 1, true).Location;
                VariablesHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 1, true).Size;

                this.VarValListView.Controls.Add(DatasHighlihterCell);
                DatasHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 2, true).Location;
                DatasHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 2, true).Size;

                this.VarValListView.Controls.Add(SeparatorsHighlihterCell);
                SeparatorsHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 3, true).Location;
                SeparatorsHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 3, true).Size;


                this.VarValListView.Controls.Add(AnswerHighlihterCell);
                AnswerHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 4, true).Location;
                AnswerHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 4, true).Size;

                this.VarValListView.Controls.Add(AnswerErrorHighlihterCell);
                AnswerErrorHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 5, true).Location;
                AnswerErrorHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 5, true).Size;

                this.VarValListView.Controls.Add(ExceptionWordHighlihterCell);
                ExceptionWordHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 6, true).Location;
                ExceptionWordHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 6, true).Size;

                this.VarValListView.Controls.Add(ErrorHighlighterCell);
                ErrorHighlighterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 7, true).Location;
                ErrorHighlighterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 7, true).Size;

                this.VarValListView.Controls.Add(PopupMenuBackColorCell);
                PopupMenuBackColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 8, true).Location;
                PopupMenuBackColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 8, true).Size;

                this.VarValListView.Controls.Add(PopupMenuSelectionBackColorCell);
                PopupMenuSelectionBackColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 9, true).Location;
                PopupMenuSelectionBackColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 9, true).Size;

                this.VarValListView.Controls.Add(PopupMenuSelectionForeColorCell);
                PopupMenuSelectionForeColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 10, true).Location;
                PopupMenuSelectionForeColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 10, true).Size;


                foreach (DataGridViewRow row in VarValListView.Rows)
                {
                    row.Cells[0].ReadOnly = true;
                    row.Cells[1].ReadOnly = true;
                }
            }




        }

        private void VarValListView_Scroll(object sender, ScrollEventArgs e)
        {
            if (VarSelection.SelectedIndex == 0)
            {
                TableLayoutPanel textColorCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_textColor", true)[0];
                textColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 2, true).Location;
                textColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 2, true).Size;

                TableLayoutPanel textBackColorCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_textBackColor", true)[0];
                textBackColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 3, true).Location;
                textBackColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 3, true).Size;

                TableLayoutPanel menuColorCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_menuColor", true)[0];
                menuColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 5, true).Location;
                menuColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 5, true).Size;

                TableLayoutPanel menuBackColorCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_menuBackColor", true)[0];
                menuBackColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 6, true).Location;
                menuBackColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 6, true).Size;
            }else if (VarSelection.SelectedIndex == 1)
            {
                TableLayoutPanel ModulesHighlihterCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_ModulesHighlihter", true)[0];
                ModulesHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 0, true).Location;
                ModulesHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 0, true).Size;

                TableLayoutPanel VariablesHighlihterCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_VariablesHighlihter", true)[0];
                VariablesHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 1, true).Location;
                VariablesHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 1, true).Size;

                TableLayoutPanel DatasHighlihterCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_DatasHighlihter", true)[0];
                DatasHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 2, true).Location;
                DatasHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 2, true).Size;

                TableLayoutPanel SeparatorsHighlihterCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_SeparatorsHighlihter", true)[0];
                SeparatorsHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 3, true).Location;
                SeparatorsHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 3, true).Size;

                TableLayoutPanel AnswerHighlihterCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_AnswerHighlihter", true)[0];
                AnswerHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 4, true).Location;
                AnswerHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 4, true).Size;

                TableLayoutPanel AnswerErrorHighlihterCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_AnswerErrorHighlihter", true)[0];
                AnswerErrorHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 5, true).Location;
                AnswerErrorHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 5, true).Size;
                
                TableLayoutPanel ExceptionWordHighlihterCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_ExceptionWordHighlihter", true)[0];
                ExceptionWordHighlihterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 6, true).Location;
                ExceptionWordHighlihterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 6, true).Size;
                
                TableLayoutPanel ErrorHighlighterCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_ErrorHighlighter", true)[0];
                ErrorHighlighterCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 7, true).Location;
                ErrorHighlighterCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 7, true).Size;



                TableLayoutPanel PopupMenuBackColorCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_PopupMenuBackColor", true)[0];
                PopupMenuBackColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 8, true).Location;
                PopupMenuBackColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 8, true).Size;

                TableLayoutPanel PopupMenuSelectionBackColorCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_PopupMenuSelectionBackColor", true)[0];
                PopupMenuSelectionBackColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 9, true).Location;
                PopupMenuSelectionBackColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 9, true).Size;


                TableLayoutPanel PopupMenuSelectionForeColorCell = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_PopupMenuSelectionForeColor", true)[0];
                PopupMenuSelectionForeColorCell.Location = this.VarValListView.GetCellDisplayRectangle(1, 10, true).Location;
                PopupMenuSelectionForeColorCell.Size = this.VarValListView.GetCellDisplayRectangle(1, 10, true).Size;



            }
        }

        private Color recieveColor(Color inColor)
        {
            Color res = inColor;
            ColorDialog cd = new ColorDialog();
            cd.Color = inColor;
            cd.AnyColor = true;
            cd.AllowFullOpen = true;
            cd.FullOpen = true;
            if(cd.ShowDialog() == DialogResult.OK)
            {
                res = cd.Color;
            }
            return res;
        }

        private Font recieveFont(Font inFont)
        {
            Font res = inFont;
            FontDialog fd = new FontDialog();
            fd.Font = inFont;
            fd.FontMustExist = true;
            fd.MinSize = 6;
            fd.ShowApply = false;
            if (fd.ShowDialog() == DialogResult.OK)
            {
                res = fd.Font;
            }

            return res;
        }

        private void VarValListView_Click(object sender, EventArgs e)
        {
            Control csender = (Control)sender;
            if (!csender.Name.Contains("_")) { return; }

            TableLayoutPanel pan;
            String name = csender.Name.Split('_')[1];

            pan = (TableLayoutPanel)VarValListView.Controls.Find("ColourPickerLayout_"+name, true)[0];
            Button btn = (Button)pan.Controls.Find("ColourCodeDisplayColour_" + name, true)[0];
            Label lbl = (Label)pan.Controls.Find("ColourPickerRaw_" + name, true)[0];

            Color color = btn.BackColor;
            Color ncolor = recieveColor(color);
            btn.BackColor = ncolor;
            lbl.Text = ColorTranslator.ToHtml(ncolor);
        }

        private void VarValListView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 1) { return; }
            if (VarSelection.SelectedIndex != 0) { return; }
            Font nFont;
            switch (e.RowIndex)
            {
                case 1:
                    nFont = recieveFont(VarValListView.Rows[1].Cells[1].Style.Font);
                    VarValListView.Rows[1].Cells[1].Style.Font = nFont;
                    VarValListView.Rows[1].Cells[1].Value = nFont.FontFamily.Name + ", " + nFont.Size;
                    break;
                case 4:
                    nFont = recieveFont(VarValListView.Rows[4].Cells[1].Style.Font);
                    VarValListView.Rows[4].Cells[1].Style.Font = nFont;
                    VarValListView.Rows[4].Cells[1].Value = nFont.FontFamily.Name + ", " + nFont.Size;
                    break;

                default:
                    break;

            }
        }

        private void VarValListView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!ready || VarSelection.SelectedIndex!=0 || e.RowIndex!=0 || e.ColumnIndex!=1) { return; }
            while (names.Contains(VarValListView.Rows[0].Cells[1].Value))
            {
                VarValListView.Rows[0].Cells[1].Value = VarValListView.Rows[0].Cells[1].Value + " - копия";
            }
        }

        private void SettingsCancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void SettingsSaveButton_Click(object sender, EventArgs e)
        {
            if(VarValListView.Rows.Count < 1) { return; }

            if (VarSelection.SelectedIndex == 1)
            {
                resultTheme.ModulesHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_ModulesHighlihter", true)[0]).BackColor;
                resultTheme.VariablesHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_VariablesHighlihter", true)[0]).BackColor;
                resultTheme.DatasHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_DatasHighlihter", true)[0]).BackColor;
                resultTheme.SeparatorsHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_SeparatorsHighlihter", true)[0]).BackColor;
                resultTheme.AnswerHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_AnswerHighlihter", true)[0]).BackColor;
                resultTheme.AnswerErrorHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_AnswerErrorHighlihter", true)[0]).BackColor;
                resultTheme.ExceptionWordHighlihter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_ExceptionWordHighlihter", true)[0]).BackColor;
                resultTheme.ErrorHighlighter = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_ErrorHighlighter", true)[0]).BackColor;


                resultTheme.PopupMenuBackColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_PopupMenuBackColor", true)[0]).BackColor;
                resultTheme.PopupMenuSelectionBackColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_PopupMenuSelectionBackColor", true)[0]).BackColor;
                resultTheme.PopupMenuSelectionForeColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_PopupMenuSelectionForeColor", true)[0]).BackColor;

            }
            else if (VarSelection.SelectedIndex == 0)
            {
                resultTheme.name = (String)VarValListView.Rows[0].Cells[1].Value;
                resultTheme.textColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_textColor", true)[0]).BackColor;
                resultTheme.textBackColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_textBackColor", true)[0]).BackColor;
                resultTheme.menuColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_menuColor", true)[0]).BackColor;
                resultTheme.menuBackColor = ((Button)VarValListView.Controls.Find("ColourCodeDisplayColour_menuBackColor", true)[0]).BackColor;

                resultTheme.textFont = VarValListView.Rows[1].Cells[1].Style.Font;
                resultTheme.menuFont = VarValListView.Rows[4].Cells[1].Style.Font;
            }

            this.DialogResult = DialogResult.OK;
        }
    }
}
