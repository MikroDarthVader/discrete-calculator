﻿namespace Guinea.GUI.Forms
{
    partial class EditThemeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.headerControlsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.VarSelection = new System.Windows.Forms.ComboBox();
            this.FieldsLabel = new System.Windows.Forms.Label();
            this.VarValListView = new System.Windows.Forms.DataGridView();
            this.Var = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.footerButtonsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SettingsSaveButton = new System.Windows.Forms.Button();
            this.SettingsCancelButton = new System.Windows.Forms.Button();
            this.headerControlsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VarValListView)).BeginInit();
            this.footerButtonsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // headerControlsPanel
            // 
            this.headerControlsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.headerControlsPanel.ColumnCount = 2;
            this.headerControlsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.headerControlsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.headerControlsPanel.Controls.Add(this.VarSelection, 1, 0);
            this.headerControlsPanel.Controls.Add(this.FieldsLabel, 0, 0);
            this.headerControlsPanel.Location = new System.Drawing.Point(12, 13);
            this.headerControlsPanel.Name = "headerControlsPanel";
            this.headerControlsPanel.RowCount = 1;
            this.headerControlsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.headerControlsPanel.Size = new System.Drawing.Size(500, 30);
            this.headerControlsPanel.TabIndex = 0;
            // 
            // VarSelection
            // 
            this.VarSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VarSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.VarSelection.FormattingEnabled = true;
            this.VarSelection.Items.AddRange(new object[] {
            "Общие",
            "Синтаксис"});
            this.VarSelection.Location = new System.Drawing.Point(130, 3);
            this.VarSelection.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.VarSelection.MaxDropDownItems = 2;
            this.VarSelection.Name = "VarSelection";
            this.VarSelection.Size = new System.Drawing.Size(370, 21);
            this.VarSelection.TabIndex = 5;
            this.VarSelection.SelectedIndexChanged += new System.EventHandler(this.VarSelection_SelectedIndexChanged);
            // 
            // FieldsLabel
            // 
            this.FieldsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FieldsLabel.AutoSize = true;
            this.FieldsLabel.Location = new System.Drawing.Point(3, 0);
            this.FieldsLabel.Name = "FieldsLabel";
            this.FieldsLabel.Size = new System.Drawing.Size(124, 30);
            this.FieldsLabel.TabIndex = 0;
            this.FieldsLabel.Text = "Параметры";
            this.FieldsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VarValListView
            // 
            this.VarValListView.AllowUserToAddRows = false;
            this.VarValListView.AllowUserToDeleteRows = false;
            this.VarValListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VarValListView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VarValListView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Var,
            this.Value});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.VarValListView.DefaultCellStyle = dataGridViewCellStyle3;
            this.VarValListView.Location = new System.Drawing.Point(12, 49);
            this.VarValListView.Name = "VarValListView";
            this.VarValListView.RowHeadersVisible = false;
            this.VarValListView.RowTemplate.Height = 33;
            this.VarValListView.ShowCellErrors = false;
            this.VarValListView.ShowCellToolTips = false;
            this.VarValListView.ShowEditingIcon = false;
            this.VarValListView.ShowRowErrors = false;
            this.VarValListView.Size = new System.Drawing.Size(500, 422);
            this.VarValListView.TabIndex = 1;
            this.VarValListView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.VarValListView_CellClick);
            this.VarValListView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.VarValListView_CellValueChanged);
            this.VarValListView.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VarValListView_Scroll);
            // 
            // Var
            // 
            this.Var.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Var.HeaderText = "Поле";
            this.Var.Name = "Var";
            this.Var.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Var.Width = 39;
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.HeaderText = "Значение";
            this.Value.Name = "Value";
            this.Value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // footerButtonsPanel
            // 
            this.footerButtonsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.footerButtonsPanel.ColumnCount = 5;
            this.footerButtonsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.footerButtonsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.footerButtonsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.footerButtonsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.footerButtonsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.footerButtonsPanel.Controls.Add(this.SettingsSaveButton, 4, 0);
            this.footerButtonsPanel.Controls.Add(this.SettingsCancelButton, 2, 0);
            this.footerButtonsPanel.Location = new System.Drawing.Point(12, 477);
            this.footerButtonsPanel.Name = "footerButtonsPanel";
            this.footerButtonsPanel.RowCount = 1;
            this.footerButtonsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.footerButtonsPanel.Size = new System.Drawing.Size(500, 22);
            this.footerButtonsPanel.TabIndex = 14;
            // 
            // SettingsSaveButton
            // 
            this.SettingsSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsSaveButton.AutoSize = true;
            this.SettingsSaveButton.Location = new System.Drawing.Point(400, 0);
            this.SettingsSaveButton.Margin = new System.Windows.Forms.Padding(0);
            this.SettingsSaveButton.Name = "SettingsSaveButton";
            this.SettingsSaveButton.Size = new System.Drawing.Size(100, 23);
            this.SettingsSaveButton.TabIndex = 9;
            this.SettingsSaveButton.Text = "Сохранить";
            this.SettingsSaveButton.UseVisualStyleBackColor = true;
            this.SettingsSaveButton.Click += new System.EventHandler(this.SettingsSaveButton_Click);
            // 
            // SettingsCancelButton
            // 
            this.SettingsCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsCancelButton.AutoSize = true;
            this.SettingsCancelButton.Location = new System.Drawing.Point(305, 0);
            this.SettingsCancelButton.Margin = new System.Windows.Forms.Padding(0);
            this.SettingsCancelButton.Name = "SettingsCancelButton";
            this.SettingsCancelButton.Size = new System.Drawing.Size(75, 23);
            this.SettingsCancelButton.TabIndex = 10;
            this.SettingsCancelButton.Text = "Отмена";
            this.SettingsCancelButton.UseVisualStyleBackColor = true;
            this.SettingsCancelButton.Click += new System.EventHandler(this.SettingsCancelButton_Click);
            // 
            // EditThemeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 511);
            this.Controls.Add(this.footerButtonsPanel);
            this.Controls.Add(this.VarValListView);
            this.Controls.Add(this.headerControlsPanel);
            this.MinimumSize = new System.Drawing.Size(540, 550);
            this.Name = "EditThemeForm";
            this.Text = "Настройка темы";
            this.Load += new System.EventHandler(this.EditThemeForm_Load);
            this.headerControlsPanel.ResumeLayout(false);
            this.headerControlsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VarValListView)).EndInit();
            this.footerButtonsPanel.ResumeLayout(false);
            this.footerButtonsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel headerControlsPanel;
        private System.Windows.Forms.Label FieldsLabel;
        private System.Windows.Forms.ComboBox VarSelection;
        private System.Windows.Forms.DataGridView VarValListView;
        private System.Windows.Forms.TableLayoutPanel footerButtonsPanel;
        private System.Windows.Forms.Button SettingsSaveButton;
        private System.Windows.Forms.Button SettingsCancelButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn Var;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
    }
}