﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Guinea.GUI.Forms.CustomControls
{
    public class CustomColourPicker
    {
        private TableLayoutPanel ColourPickerLayout;
        private Button ColourCodeDisplayColour;
        private Label ColourPickerRaw;




        public CustomColourPicker(Color colour, String name)
        {

            this.ColourPickerLayout = new TableLayoutPanel();
            this.ColourCodeDisplayColour = new Button();
            this.ColourPickerRaw = new Label();

            this.ColourPickerLayout.BackColor = System.Drawing.Color.Transparent;
            this.ColourPickerLayout.ColumnCount = 2;
            this.ColourPickerLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 32F));
            this.ColourPickerLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 308F));
            this.ColourPickerLayout.Controls.Add(this.ColourCodeDisplayColour, 0, 0);
            this.ColourPickerLayout.Controls.Add(this.ColourPickerRaw, 1, 0);
            this.ColourPickerLayout.Location = new System.Drawing.Point(71, 116);
            this.ColourPickerLayout.Name = "ColourPickerLayout_"+name;
            this.ColourPickerLayout.RowCount = 1;
            this.ColourPickerLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 32F));
            this.ColourPickerLayout.Size = new System.Drawing.Size(340, 32);
            this.ColourPickerLayout.TabIndex = 15;
            // 
            // ColourCodeDisplayColour
            // 
            this.ColourCodeDisplayColour.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom)
            | AnchorStyles.Left)
            | AnchorStyles.Right)));
            this.ColourCodeDisplayColour.BackColor = colour;
            this.ColourCodeDisplayColour.FlatStyle = FlatStyle.Flat;
            this.ColourCodeDisplayColour.Location = new System.Drawing.Point(3, 3);
            this.ColourCodeDisplayColour.Name = "ColourCodeDisplayColour_" + name;
            this.ColourCodeDisplayColour.Size = new System.Drawing.Size(26, 26);
            this.ColourCodeDisplayColour.TabIndex = 1;
            this.ColourCodeDisplayColour.UseVisualStyleBackColor = false;
            // 
            // ColourPickerRaw
            // 
            this.ColourPickerRaw.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom)
            | AnchorStyles.Left)
            | AnchorStyles.Right)));
            this.ColourPickerRaw.AutoSize = true;
            this.ColourPickerRaw.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ColourPickerRaw.ForeColor = Color.Black;
            this.ColourPickerRaw.Location = new System.Drawing.Point(35, 0);
            this.ColourPickerRaw.Name = "ColourPickerRaw_" + name;
            this.ColourPickerRaw.Size = new System.Drawing.Size(302, 32);
            this.ColourPickerRaw.TabIndex = 2;
            this.ColourPickerRaw.Text = ColorTranslator.ToHtml(colour);
            this.ColourPickerRaw.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        }
        
        public void addClickHandler(EventHandler handler)
        {
            this.ColourPickerLayout.Click += handler;
            this.ColourPickerRaw.Click += handler;
            this.ColourCodeDisplayColour.Click += handler;
        }



        public TableLayoutPanel getControls()
        {
            return this.ColourPickerLayout;
        }
    }
}
