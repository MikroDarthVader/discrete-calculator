﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Guinea.GUI
{
    public class Theme
    {
        public static Theme currentTheme { get; set; }
        public static List<Theme> themes = new List<Theme>();

        public Theme() : this(1) { }

        public Theme(int theme)
        {
            if (theme == 0)
            {
                //light theme
                this.name = "Default Light theme";

                this.textFont = new Font(new FontFamily("Consolas"), 13);
                this.textColor = Color.Black;
                this.textBackColor = Color.White;

                this.menuFont = new Font(new FontFamily("Consolas"), 10);
                this.menuColor = Color.Black;
                this.menuBackColor = Color.White;

                //this.inOutFont = new Font(new FontFamily("Consolas"), 10);
                //this.inOutBackColor = Color.White;

                this.ModulesHighlihter = Color.Green;
                this.VariablesHighlihter = Color.Blue;
                this.DatasHighlihter = Color.Orange;
                this.SeparatorsHighlihter = Color.Black;
                this.AnswerHighlihter = Color.BlueViolet;
                this.AnswerErrorHighlihter = Color.IndianRed;
                this.ExceptionWordHighlihter = Color.Red;
                this.ErrorHighlighter = Color.Red;

                this.PopupMenuBackColor = Color.White;
                this.PopupMenuSelectionBackColor = Color.Blue;
                this.PopupMenuSelectionForeColor = Color.White;

            }
            else
            {
                //dark theme
                this.name = "Default Dark theme";

                this.textFont = new Font(new FontFamily("Consolas"), 13);
                this.textColor = Color.White;
                this.textBackColor = Color.FromArgb(20, 24, 28);

                this.menuFont = new Font(new FontFamily("Consolas"), 10);
                this.menuColor = Color.White;
                this.menuBackColor = Color.FromArgb((int)(20 * 1.5), (int)(24 * 1.5), (int)(33 * 1.5));

                this.ModulesHighlihter = Color.LightGreen;
                this.VariablesHighlihter = Color.FromArgb(250, 102, 217);
                this.DatasHighlihter = Color.Orange;
                this.SeparatorsHighlihter = Color.White;
                this.AnswerHighlihter = Color.FromArgb(110, 185, 255);
                this.AnswerErrorHighlihter = Color.FromArgb(255, 74, 70);
                this.ExceptionWordHighlihter = Color.Red;
                this.ErrorHighlighter = Color.FromArgb(255, 74, 70);

                this.PopupMenuBackColor = this.menuBackColor;
                this.PopupMenuSelectionBackColor = Color.Orange;
                this.PopupMenuSelectionForeColor = Color.White;
            }
        }

        public Theme(Theme original)
        {
            this.name = original.name;
            this.textFont = original.textFont;
            this.textBackColor = original.textBackColor;
            this.textColor = original.textColor;

            this.menuFont = original.menuFont;
            this.menuBackColor = original.menuBackColor;
            this.menuColor = original.menuColor;

            //this.inOutFont = original.inOutFont;
            //this.inOutBackColor = original.inOutBackColor;

            this.ModulesHighlihter = original.ModulesHighlihter;
            this.VariablesHighlihter = original.VariablesHighlihter;
            this.DatasHighlihter = original.DatasHighlihter;
            this.SeparatorsHighlihter = original.SeparatorsHighlihter;
            this.AnswerHighlihter = original.AnswerHighlihter;
            this.AnswerErrorHighlihter = original.AnswerErrorHighlihter;
            this.ExceptionWordHighlihter = original.ExceptionWordHighlihter;
            this.ErrorHighlighter = original.ErrorHighlighter;

            this.PopupMenuBackColor = original.PopupMenuBackColor;
            this.PopupMenuSelectionBackColor = original.PopupMenuSelectionBackColor;
            this.PopupMenuSelectionForeColor = original.PopupMenuSelectionForeColor;

        }



        public String name;

        public Font textFont;
        public Color textColor;
        public Color textBackColor;

        public Font menuFont;
        public Color menuBackColor;
        public Color menuColor;

        //public Font  inOutFont              { get; set; }
        //public Color inOutBackColor         { get; set; }

        public Color ModulesHighlihter;
        public Color VariablesHighlihter;
        public Color DatasHighlihter;
        public Color SeparatorsHighlihter;
        public Color AnswerHighlihter;
        public Color AnswerErrorHighlihter;
        public Color ExceptionWordHighlihter;
        public Color ErrorHighlighter;

        public Color PopupMenuBackColor;
        public Color PopupMenuSelectionBackColor;
        public Color PopupMenuSelectionForeColor;


        public void ApplyTheme(Form form)
        {
            ApplyTheme((Control)form);
        }

        public void ApplyTheme(Control element)
        {
            if (element.HasChildren)
            {
                foreach (Control child in element.Controls)
                {
                    ApplyTheme(child);
                }
            }
            if (element.Tag == null)
            {
                return;
            }
            switch (element.Tag.ToString().ToLower())
            {
                case "inout":
                    element.BackColor = this.textBackColor;
                    element.ForeColor = this.textColor;
                    ((FastConsole)element).updateColors(this);

                    break;
                case "text":
                    element.BackColor = this.textBackColor;
                    element.ForeColor = this.textColor;
                    element.Font = this.textFont;
                    break;
                case "menu":
                    element.BackColor = this.menuBackColor;
                    element.ForeColor = this.menuColor;
                    element.Font = this.menuFont;
                    break;
            }
            element.Update();

        }
    }
}
