﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastColoredTextBoxNS;
using Guinea.Environment;

namespace Guinea
{
    public class VariableAutoComplite : AutocompleteItem
    {
        private KeyValuePair<string, string> variable;
        private int maxLenght = 50;
        string Title = "Значение:";

        public VariableAutoComplite(string name, string value, Color variableColor)
        {
            Text = name;
            variable = new KeyValuePair<string, string>(name, value);
            foreColor = variableColor;
        }

        private Color foreColor;
        public override Color ForeColor
        {
            get
            {
                return foreColor;
            }
            set
            {
                foreColor = value;
            }
        }

        public void Rewritevalue(string value)
        {
            variable = new KeyValuePair<string, string>(variable.Key, value);
        }

        public override string ToolTipText
        {
            get
            {
                if (variable.Value.Length < maxLenght)
                    return variable.Value;
                else
                    return variable.Value.Substring(0, maxLenght) + "...";
            }
            set
            {
                base.ToolTipText = value;
            }
        }

        public override string ToolTipTitle
        {
            get
            {
                return Title;
            }
            set
            {
                base.ToolTipTitle = value;
            }
        }

        public override string GetTextForReplace()
        {
            return variable.Key;
        }
    }
}