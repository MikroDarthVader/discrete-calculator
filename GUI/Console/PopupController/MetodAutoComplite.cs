﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastColoredTextBoxNS;
using Guinea.Environment;
using static Guinea.FastConsole.ConsoleInputAPI;

namespace Guinea
{
    public class MetodAutoComplite : AutocompleteItem
    {
        public IModule module;
        public string TipTitle;

        public MetodAutoComplite(IModule module, Color MetodColor)
        {
            Text = module.ToString().Substring("Guinea.Modules.".Length);
            this.module = module;
            TipTitle = createTitle();
            foreColor = MetodColor;
        }

        private Color foreColor;
        public override Color ForeColor
        {
            get
            {
                return foreColor;
            }
            set
            {
                foreColor = value;
            }
        }

        public override void OnSelected(AutocompleteMenu popupMenu, SelectedEventArgs e)
        {
            var Tb = e.Tb;
            Tb.SelectionStart = Tb.SelectionStart - module.ReceiveType.Length;
        }

        public override string ToolTipText
        {
            get
            {
                return module.PrintableName;
            }
            set
            {
                base.ToolTipText = value;
            }
        }

        public override string ToolTipTitle
        {
            get
            {
                return TipTitle;
            }
            set
            {
                base.ToolTipTitle = value;
            }
        }

        public override string GetTextForReplace()
        {
            return ModifideModule(module);
        }

        public string createTitle()
        {
            string data;
            UI.PrintableDatas.TryGetValue(module.ReturnType, out string result);
            result += " " + module.ToString().Substring("Guinea.Modules.".Length) + "(";
            UI.PrintableDatas.TryGetValue(module.ReceiveType[0], out data);
            result += data;

            for (int i = 1; i < module.ReceiveType.Length; i++)
            {
                UI.PrintableDatas.TryGetValue(module.ReceiveType[i], out data);
                result += "; " + data;
            }

            result += ")";

            return result;
        }
    }
}

