﻿using System.Linq;
using System.Drawing;
using Guinea.Environment;
using System;
using FastColoredTextBoxNS;

namespace Guinea
{
    public partial class FastConsole
    {
        public static class ConsoleInputAPI
        {
            public static FastConsole Console;

            public static string ModifideModule(IModule module)
            {
                string moduleToExpression = module.GetType().ToString().Substring("Guinea.Modules.".Length);
                moduleToExpression += '(';

                int i;
                for (i = 0; i < module.ReceiveType.Length - 1; i++)
                {
                    moduleToExpression += ';';
                }
                moduleToExpression += ')';

                return moduleToExpression;
            }

            public static void SelectInLastLine(int start, int end)
            {
                Console.Selection.Start = new Place(start, Console.Lines.Count - 1);
                Console.Selection.End = new Place(end, Console.Lines.Count - 1);
            }
        }
    }
}

