﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastColoredTextBoxNS;
using Guinea.Environment;
using System.Windows.Forms;
using System.Diagnostics;
using static Guinea.FastConsole.ConsoleInputAPI;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;

namespace Guinea
{
    public partial class FastConsole : FastColoredTextBox
    {
        public AutocompleteMenu popupMenu;
        public static List<AutocompleteItem> AutoCompleteItems = new List<AutocompleteItem>();

        public TextStyle ModulesHighlihter = new TextStyle(Brushes.Green, null, FontStyle.Regular);
        public TextStyle VariablesHighlihter = new TextStyle(Brushes.Blue, null, FontStyle.Regular);
        public TextStyle DatasHighlihter = new TextStyle(Brushes.Orange, null, FontStyle.Regular);
        public TextStyle SeparatorsHighlihter = new TextStyle(Brushes.Black, null, FontStyle.Regular);
        public TextStyle AnswerHighlihter = new TextStyle(Brushes.BlueViolet, null, FontStyle.Regular);
        public TextStyle AnswerErrorHighlihter = new TextStyle(Brushes.IndianRed, null, FontStyle.Regular);
        public TextStyle ExceptionWordHighlihter = new TextStyle(Brushes.Red, null, FontStyle.Bold);
        public TextStyle ErrorHighlighter = new TextStyle(Brushes.Red, null, FontStyle.Underline);

        public Color ModulesColor = Color.Green;
        public Color VariablesColor = Color.Blue;
        public Color PopupMenuBackColor = Color.White;
        public Color PopupMenuSelectionBackColor = Color.Blue;
        public Color PopupMenuSelectionForeColor = Color.White;

        public void RecolorAllText() // перекрашивает текст и подсказки в соответствии с текущей конфигурации переменных
        {
            Task.Run(() => SyntaxHighlighting(Range));
            RewritePopupMenuItemsColor();
        }

        public void OnLoad()
        {
            LoadAutoComplete();
            BuildAutoCompleteMenu();
            Variables.Console = this;
        }

        public void updateColors(GUI.Theme theme)
        {
            this.CaretColor = Color.FromArgb(255 - BackColor.R, 255 - BackColor.G, 255 - BackColor.B);
            this.ModulesColor = theme.ModulesHighlihter;
            this.VariablesColor = theme.VariablesHighlihter;
            if (popupMenu != null)
            {
                popupMenu.BackColor = theme.PopupMenuBackColor;
                popupMenu.SelectedColor = theme.PopupMenuSelectionBackColor;
            }
            else
            {
                this.PopupMenuBackColor = theme.PopupMenuBackColor;
                this.PopupMenuSelectionBackColor = theme.PopupMenuSelectionBackColor;
            }
            this.PopupMenuSelectionForeColor = theme.PopupMenuSelectionForeColor;

            this.ModulesHighlihter.ForeBrush = new SolidBrush(this.ModulesColor);
            this.VariablesHighlihter.ForeBrush = new SolidBrush(this.VariablesColor);

            this.DatasHighlihter.ForeBrush = new SolidBrush(theme.DatasHighlihter);
            this.SeparatorsHighlihter.ForeBrush = new SolidBrush(theme.SeparatorsHighlihter);
            this.AnswerHighlihter.ForeBrush = new SolidBrush(theme.AnswerHighlihter);
            this.AnswerErrorHighlihter.ForeBrush = new SolidBrush(theme.AnswerErrorHighlihter);
            this.ErrorHighlighter.ForeBrush = new SolidBrush(theme.ErrorHighlighter);
            this.ExceptionWordHighlihter.ForeBrush = new SolidBrush(theme.ExceptionWordHighlihter);
            RecolorAllText();
    }


        internal void LoadAutoComplete()
        {
            popupMenu = new AutocompleteMenu(this);
            popupMenu.ToolTip.Active = true;
            popupMenu.ToolTip.UseFading = true;
            popupMenu.ToolTipDuration = 3600000;
            popupMenu.AllowTabKey = false;
            popupMenu.AutoClose = false;
            popupMenu.AutoSize = false;
            popupMenu.AppearInterval = int.MaxValue;
            popupMenu.ImageScalingSize = new Size(20, 20);
            popupMenu.LayoutStyle = ToolStripLayoutStyle.Flow;
            popupMenu.MinFragmentLength = 0;
            popupMenu.Name = "popupMenu";
            popupMenu.Padding = new Padding(0);
            popupMenu.SearchPattern = "[\\w\\.]";
            popupMenu.Size = new Size(154, 154);
            popupMenu.BackColor = PopupMenuBackColor;
            popupMenu.SelectedColor = PopupMenuSelectionBackColor;
            popupMenu.Items.FocussedItemIndexChanged += new EventHandler(Items_FocussedItemIndexChanged);
            popupMenu.Opening += new EventHandler<CancelEventArgs>(PopupMenu_Opening);
            popupMenu.Closing += new ToolStripDropDownClosingEventHandler(PopupMenu_Closing);
        }

        private AutocompleteItem lastItem;
        private Color lastFontColor;
        private void Items_FocussedItemIndexChanged(object sender, EventArgs e)
        {
            if (popupMenu.Items.FocussedItem != null)
            {
                lastItem.ForeColor = lastFontColor;
                lastItem = popupMenu.Items.FocussedItem;
                lastFontColor = popupMenu.Items.FocussedItem.ForeColor;
                popupMenu.Items.FocussedItem.ForeColor = PopupMenuSelectionForeColor;
            }
        }

        public bool popupOpened = false;
        private void PopupMenu_Opening(object sender, EventArgs e)
        {
            lastItem = popupMenu.Items.FocussedItem;
            lastFontColor = popupMenu.Items.FocussedItem.ForeColor;
            popupMenu.Items.FocussedItem.ForeColor = PopupMenuSelectionForeColor;
            popupOpened = true;
        }

        private void PopupMenu_Closing(object sender, EventArgs e)
        {
            lastItem.ForeColor = lastFontColor;
            popupOpened = false;
        }

        private void BuildAutoCompleteMenu()
        {
            foreach (IModule module in UI.Modules.Values)
            {
                if (module.VisibleInTree)
                    AutoCompleteItems.Add(new MetodAutoComplite(module, ModulesColor));
            }
            popupMenu.Items.SetAutocompleteItems(AutoCompleteItems);
        }

        public void NewVariableAction(string variableName, string value)
        {
            try
            {
                (AutoCompleteItems.Find(x => x.GetType() == typeof(VariableAutoComplite) && x.Text == variableName) as VariableAutoComplite).Rewritevalue(value);
            }
            catch
            {
                AutoCompleteItems.Add(new VariableAutoComplite(variableName, value, VariablesColor));
            }
        }

        private void RewritePopupMenuItemsColor()
        {
            foreach (AutocompleteItem item in AutoCompleteItems)
            {
                try
                {
                    (item as MetodAutoComplite).ForeColor = ModulesColor;
                }
                catch
                {
                    (item as VariableAutoComplite).ForeColor = VariablesColor;
                }
            }
        }

        public void InsertModuleInConsole(IModule module)
        {
            string moduleToExpression = ModifideModule(module);

            InsertText(moduleToExpression);
            Selection.Start = new Place(Selection.Start.iChar - module.ReceiveType.Length, LinesCount - 1);
        }

        public void StopCalculating()
        {
            if (Calculating != null && Calculating.ThreadState == System.Threading.ThreadState.Running && !PrintedAnswer)
            {
                Calculating.Abort();
                currentHistoryLine = currHistoryInsertLine = History.Count - 1;
                ReadOnly = false;
                PrintedAnswer = true;
                AppendText("\n >>> Exception: Вычисления прерваны пользователем\0\n");
                Selection.Start = Range.End;
            }
        }

        Thread Calculating;
        Place StartReadPlace = new Place(0, 0);
        bool PrintedAnswer = false;
        public override void OnTextChanging(ref string text)
        {
            if (text == ">")
            {
                text = "";
            }
            else if (text == "\n" && !ReadOnly)
            {
                if (Lines[LinesCount - 1].Length == 0)
                {
                    text = "";
                    Selection.Start = StartReadPlace;
                    return;
                }
                else
                {
                    ChangedHistoryElements.Clear();
                    text = "";
                    Calculating = new Thread(() => PrintAnswer());
                    Calculating.Start();
                    return;
                }
            }
            else
            {
                if (Selection.Start < StartReadPlace)
                {
                    text = "";
                    Selection.Start = StartReadPlace;
                }
                if (Selection.Length == 0 && Selection.Start == StartReadPlace)
                {
                    if (text == "\b") //backspace
                    {
                        text = ""; //cancel deleting of last char of readonly text
                    }
                    Selection.Start = StartReadPlace;
                }
            }
        }

        public void Console_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Text.Length == 0 && StartReadPlace != Range.Start)
            {
                StartReadPlace = Range.Start;
                History.Clear();
                History.Add("");
                ChangedHistoryElements.Clear();
            }

            if (PrintedAnswer)
            {
                StartReadPlace = Range.End;
                Selection.Start = StartReadPlace;
                PrintedAnswer = false;
            }
            Task.Run(() => SyntaxHighlighting(e.ChangedRange));
        }

        private void SyntaxHighlighting(Range range)
        {
            try
            {
                range.ClearStyle(SeparatorsHighlihter,
                VariablesHighlihter,
                ModulesHighlihter,
                DatasHighlihter,
                AnswerErrorHighlihter,
                AnswerHighlihter,
                ExceptionWordHighlihter,
                ErrorHighlighter);

                range.SetStyle(SeparatorsHighlihter, @"\s*[;)(=>]");
                range.SetStyle(ExceptionWordHighlihter, @" >>> Exception:");
                range.SetStyle(AnswerErrorHighlihter, @" >>> Exception:.*\0");
                range.SetStyle(AnswerHighlihter, @" >>> (\s|\/|\^|\*|-|\+|\w|\n)*\0");
                range.SetStyle(DatasHighlihter, UI.DatasRegex);
                range.SetStyle(VariablesHighlihter, Variables.GetBeforeEqualRegex());
                range.SetStyle(VariablesHighlihter, Variables.GetRegex());
                range.SetStyle(ModulesHighlihter, UI.ModulesRegex);
                range.SetStyle(ErrorHighlighter, ".*");
            }
            catch (Exception exception)
            {
#if DEBUG
                Debug.WriteLine("Can't color text, because it was changed: (" + exception.Message + ")");
#endif
            }
        }

        public void PrintAnswer()
        {
            ReadOnly = true;
            History.Insert(History.Count - 1, Lines.Last());
            try
            {
                StringBuilder input = new StringBuilder(GetRange(StartReadPlace, new Place(Lines.Last().Length, LinesCount - 1)).Text);
                ans.Clear();
                lastSep = -1;
                lastLineBreak = -1;
                UI.TryParseAsData(input.Replace("\r", "").Replace("\n", "").ToString()).PrintIn(printChar);
                string answer = ans.ToString();
                PrintedAnswer = true;
                AppendText("\n >>> " + answer + "\0\n");
                StringBuilder selectionText = new StringBuilder(answer);
                History.Insert(History.Count - 1, selectionText.Replace("\r", "").Replace("\n", "").ToString());
            }
            catch (UserException e)
            {
                PrintedAnswer = true;
                AppendText("\n >>> Exception: " + e.Message + "\0\n");
            }
            currentHistoryLine = currHistoryInsertLine = History.Count - 1;
            Selection.Start = Range.End;
            ClearUndo();
            ReadOnly = false;
            Invoke(new EventHandler(delegate
            {
                Update();
                if (VerticalScroll.Visible)
                {
                    VerticalScroll.Value = VerticalScroll.Maximum;
                    HorizontalScroll.Value = 0;
                    UpdateScrollbars();
                }
            }));
        }

        int lastSep, lastLineBreak;
        StringBuilder ans = new StringBuilder();
        private void printChar(char c)
        {
            int lineLen = ClientSize.Width / CharWidth - 3;
            int curCharPos = ans.Length + 6;
            ans.Append(c);
            if (c == '+' || c == '-')
                lastSep = curCharPos;

            if (curCharPos - lastLineBreak >= lineLen)
            {
                if (curCharPos - lastSep > lineLen || lastSep < 0)
                {
                    ans.Append('\n');
                    lastLineBreak = curCharPos;
                }
                else
                {
                    ans.Insert(lastSep - 5, '\n');
                    lastLineBreak = lastSep;
                }
            }

        }

        private static int currHistoryInsertLine;
        private static int currentHistoryLine;
        private static List<string> History = new List<string>() { "" };
        private static Dictionary<int, string> ChangedHistoryElements = new Dictionary<int, string>();
        private string temp;

        public void HistoryUp(bool shiftPressed)
        {
            try
            {
                if (!popupOpened && !ReadOnly)
                {
                    if (!shiftPressed && currentHistoryLine > 0)
                    {
                        Selection.Start = StartReadPlace;
                        Selection.End = new Place(Lines.Last().Length, LinesCount - 1);

                        StringBuilder selectionText = new StringBuilder(Selection.Text);
                        string selectedText = selectionText.Replace("\r", "").Replace("\n", "").ToString();
                        if (History[currentHistoryLine] != selectedText)
                        {
                            if (!ChangedHistoryElements.ContainsKey(currentHistoryLine))
                                ChangedHistoryElements.Add(currentHistoryLine, selectedText);
                            else
                                ChangedHistoryElements[currentHistoryLine] = selectedText;
                        }

                        currentHistoryLine--;

                        if (ChangedHistoryElements.TryGetValue(currentHistoryLine, out temp))
                            InsertText(temp);
                        else
                            InsertText(History[currentHistoryLine]);

                        HorizontalScroll.Value = 0;
                        UpdateScrollbars();
                        Selection.Start = Selection.End;
                    }
                    else if (shiftPressed && currHistoryInsertLine > 0)
                    {
                        currHistoryInsertLine--;
                        Place start = Selection.Start;
                        InsertText(History[currHistoryInsertLine]);
                        Place end = Selection.Start;
                        Selection.Start = start;
                        Selection.End = end;
                    }
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("Can't iterate up in history, because console text was changed: (" + e.Message + ")");
#endif
            }
        }

        public void HistoryDown(bool shiftPressed)
        {
            try
            {
                if (!popupOpened && !ReadOnly)
                {
                    if (!shiftPressed && currentHistoryLine < History.Count - 1)
                    {
                        Selection.Start = StartReadPlace;
                        Selection.End = new Place(Lines.Last().Length, LinesCount - 1);

                        StringBuilder selectionText = new StringBuilder(Selection.Text);
                        string selectedText = selectionText.Replace("\r", "").Replace("\n", "").ToString();
                        if (History[currentHistoryLine] != selectedText)
                        {
                            if (!ChangedHistoryElements.ContainsKey(currentHistoryLine))
                                ChangedHistoryElements.Add(currentHistoryLine, selectedText);
                            else
                                ChangedHistoryElements[currentHistoryLine] = selectedText;
                        }

                        currentHistoryLine++;

                        if (ChangedHistoryElements.TryGetValue(currentHistoryLine, out temp))
                            InsertText(temp);
                        else
                            InsertText(History[currentHistoryLine]);

                        HorizontalScroll.Value = 0;
                        UpdateScrollbars();
                        Selection.Start = Selection.End;
                    }
                    else if (shiftPressed && currHistoryInsertLine < History.Count - 1)
                    {
                        currHistoryInsertLine++;
                        Place start = Selection.Start;
                        InsertText(History[currHistoryInsertLine]);
                        Place end = Selection.Start;
                        Selection.Start = start;
                        Selection.End = end;
                    }
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("Can't iterate down in history, because console text was changed: (" + e.Message + ")");
#endif
            }
        }

        public void ShiftStateChange(bool shiftPressed)
        {
            this.shiftPressed = shiftPressed;
        }

        private bool shiftPressed = false;

        public void ResetHistoryPos()
        {
            if (!shiftPressed && !popupOpened)
            {
                currHistoryInsertLine = History.Count - 1;
            }
        }
    }
}
