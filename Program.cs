﻿using System;
using Guinea.Modules;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using Guinea.Environment;

namespace Guinea
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            foreach (Type module in GetImplementers(typeof(IModule)))
            {
                UI.Modules.Add(module.GetHashCode(), (IModule)module.GetConstructor(new Type[0]).Invoke(new object[0]));
            }

            UI.ModulesRegex = @"\s*(";
            foreach (IModule module in UI.Modules.Values)
            {
                UI.ModulesRegex += module.ToString().Remove(0, "Guinea.Modules.".Length) + "|";
            }
            UI.ModulesRegex = UI.ModulesRegex.Substring(0, UI.ModulesRegex.Length - 1) + @")\s*\("; // надо пофиксить - не матчит модули в скобках

            foreach (Type data in GetImplementers(typeof(IData)))
            {
                UI.Datas.Add(data.GetHashCode(), (IData)data.GetConstructor(new Type[0]).Invoke(new object[0]));
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        public static Type[] GetChild(Type type)
        {
            return Assembly.GetAssembly(type).GetTypes().Where(t => t.IsSubclassOf(type)).ToArray();
        }

        public static Type[] GetImplementers(Type type)
        {
            return Assembly.GetExecutingAssembly().GetTypes().Where(x => x.GetInterface(type.ToString()) != null).ToArray();
        }

        public static object InvokeStatic(Type t, string method, params object[] parameters)
        {
            var methodInfo = t.GetMethod(method);
            if (methodInfo != null)
            {
                return methodInfo.Invoke(null, parameters); //null - means calling static method
            }
            return null;
        }
    }
}
