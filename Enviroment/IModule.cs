﻿using System;

namespace Guinea.Environment
{
    public interface IModule
    {
        string PrintableName { get; }
        bool VisibleInTree { get; }
        moduleType Type { get; }
        string[] Hints { get; }
        Type[] ReceiveType { get; }
        Type ReturnType { get; }
        IData Calculate(params IData[] data);
    }
}