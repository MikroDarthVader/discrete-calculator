﻿namespace Guinea.Environment
{
    public delegate void PrintMethod(char str);
    public interface IData
    {
        string GetString();
        IData TryParse(string str);
        void PrintIn(PrintMethod print);
    }
}
