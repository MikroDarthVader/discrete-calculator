﻿using Guinea.GUI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guinea.Environment
{
    public class Config
    {
        public static String path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData)+ Path.DirectorySeparatorChar + "Guinea"; // ~ ~ ~ c:\users\user\appdata\local
        public String selectedTheme;
        public Boolean showOnlyRequested;
        public static Config cfg = new Config();

        public Config()
        {
            showOnlyRequested = true;
            selectedTheme = "Default Light theme";
        }
        
        public void save()
        {
            this.selectedTheme = Theme.currentTheme.name;


            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (File.Exists(path + Path.DirectorySeparatorChar + "config.json"))
            {
                File.Delete(path + Path.DirectorySeparatorChar + "config.json");
            }
            File.WriteAllText(path+ Path.DirectorySeparatorChar+"config.json", JsonConvert.SerializeObject(this, Formatting.Indented));
        }
        
    }
}
