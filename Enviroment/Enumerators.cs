﻿namespace Guinea.Environment
{
    public enum moduleType
    {
        natural,
        integer,
        rational,
        polynomial
    }

    public enum signature
    {
        plus,
        minus
    }

    public static class Messages
    {
        public static string bigger = "Больше",
                      less = "Меньше",
                      equal = "Равно",
                      positive = "Положительно",
                      negative = "Отрицательно",
                      zero = "Ноль",
                      yes = "Да",
                      no = "Нет";
    }
}