﻿using System;
using Guinea.Modules;


namespace Guinea.Environment
{
    public class Integer : IData
    {
        public signature sign;
        public Natural number;
        public int Length { get { return number.Length; } }

        public Integer() { number = new Natural(); }

        public Integer(signature sign, params sbyte[] digits)
        {
            this.sign = sign;
            number = new Natural(digits);
        }

        public Integer Clone()
        {
            return new Integer(sign, number.Clone());
        }

        public Integer(signature sign, Natural number)
        {
            this.sign = sign;
            this.number = number;
        }

        public void Insert(int index, sbyte digit)
        {
            number.Insert(index, digit);
        }

        public void Remove(int index)
        {
            number.Remove(index);
        }

        public sbyte this[int index]
        {
            get
            {
                return number[index];
            }
            set
            {
                number[index] = value;
            }
        }

        public static Integer operator +(Integer left, Integer right)
        {
            return UI.FindModule(typeof(ADD_ZZ_Z)).Calculate(left, right) as Integer;
        }

        public static Integer operator -(Integer left, Integer right)
        {
            return UI.FindModule(typeof(SUB_ZZ_Z)).Calculate(left, right) as Integer;
        }

        public static Integer operator *(Integer left, Integer right)
        {
            return UI.FindModule(typeof(MUL_ZZ_Z)).Calculate(left, right) as Integer;
        }

        public static Integer operator /(Integer left, Natural right)
        {
            return UI.FindModule(typeof(DIV_ZZ_Z)).Calculate(left, right) as Integer;
        }

        public static Integer operator %(Integer left, Natural right)
        {
            return UI.FindModule(typeof(MOD_ZZ_Z)).Calculate(left, right) as Integer;
        }

        public string GetString()
        {
            return (sign == signature.minus && !number.Equals(Natural.zero) ? "-" : "") + number.GetString();
        }

        public IData TryParse(string str)
        {
            if (str == null || str.Length == 0)
                return null;

            signature sign;
            Natural number = UI.FindData(typeof(Natural)).TryParse(str) as Natural;

            if (str[0] == '+')
                sign = signature.plus;
            else if (str[0] == '-')
                sign = signature.minus;
            else if (number != null)
                return UI.FindModule(typeof(TRANS_N_Z)).Calculate(number);
            else return null;

            number = UI.FindData(typeof(Natural)).TryParse(str.Substring(1)) as Natural;

            if (number != null)
                return new Integer(sign, number);
            else
                return null;
        }

        public void PrintIn(PrintMethod print)
        {
            if (sign == signature.minus && !number.Equals(Natural.zero))
                print('-');
            number.PrintIn(print);
        }
    }
}
