﻿using System;
using System.Collections.Generic;
using System.Linq;
using Guinea.Modules;

namespace Guinea.Environment
{
    public class Natural : IData
    {
        protected List<sbyte> digits;
        public int Length { get { return digits.Count; } }

        public static Natural zero = new Natural(0);

        public Natural() { digits = new List<sbyte>(); }

        public Natural(params sbyte[] digits)
        {
            this.digits = new List<sbyte>(digits);
        }

        public Natural Clone()
        {
            return new Natural(digits.ToArray());
        }

        public void Insert(int index, sbyte digit)
        {
            digits.Insert(index, digit);
        }

        public void Remove(int Index)
        {
            digits.RemoveAt(Index);
        }

        public sbyte this[int index]
        {
            get
            {
                return digits[index];
            }
            set
            {
                digits[index] = value;
            }
        }

        public static Natural operator +(Natural left, Natural right)
        {
            return UI.FindModule(typeof(ADD_NN_N)).Calculate(left, right) as Natural;
        }

        public static Natural operator -(Natural left, Natural right)
        {
            return UI.FindModule(typeof(SUB_NN_N)).Calculate(left, right) as Natural;
        }

        public static Natural operator ++(Natural left)
        {
            return UI.FindModule(typeof(ADD_1N_N)).Calculate(left) as Natural;
        }

        public static Natural operator *(Natural left, Natural right)
        {
            return UI.FindModule(typeof(MUL_NN_N)).Calculate(left, right) as Natural;
        }

        public static Natural operator /(Natural left, Natural right)
        {
            return UI.FindModule(typeof(DIV_NN_N)).Calculate(left, right) as Natural;
        }

        public static Natural operator %(Natural left, Natural right)
        {
            return UI.FindModule(typeof(MOD_NN_N)).Calculate(left, right) as Natural;
        }

        public static bool operator <(Natural left, Natural right)
        {
            return UI.FindModule(typeof(COM_NN_D)).Calculate(left, right).GetString() == Messages.less;
        }

        public static bool operator >(Natural left, Natural right)
        {
            return UI.FindModule(typeof(COM_NN_D)).Calculate(left, right).GetString() == Messages.bigger;
        }

        public override bool Equals(object right)
        {
            if (right is Natural)
            {
                if (UI.FindModule(typeof(NZER_N_B)).Calculate(right as Natural).ToString() == Messages.yes && UI.FindModule(typeof(NZER_N_B)).Calculate(this).ToString() == Messages.yes)
                    return true;
                if (right != null && right is Natural)
                    return UI.FindModule(typeof(COM_NN_D)).Calculate(this, right as Natural).GetString() == Messages.equal;
            }

            return false;
        }

        public static bool operator >=(Natural left, Natural right)
        {
            return UI.FindModule(typeof(COM_NN_D)).Calculate(left, right).GetString() != Messages.less;
        }

        public static bool operator <=(Natural left, Natural right)
        {
            return UI.FindModule(typeof(COM_NN_D)).Calculate(left, right).GetString() != Messages.bigger;
        }

        public string GetString()
        {
            string result = "";

            for (int i = digits.Count - 1; i >= 0; i--)
            {
                if (digits[i] < 0 || digits[i] > 9)
                    throw new Exception("lol, really? You trying to insert " + digits[i] + " as digit in decimal number.");
                else
                    result += digits[i];
            }

            return result;
        }

        public IData TryParse(string str)
        {
            if (str == null || str.Length == 0)
                return null;

            Natural number = new Natural();

            if (str.Length > 1 && str[0] == '0')
                return null;

            for (int i = str.Length - 1; i >= 0; i--)
            {
                if (str[i] - '0' > 9 || str[i] - '0' < 0)
                    return null;

                number.Insert(str.Length - i - 1, (sbyte)(str[i] - '0'));
            }
            return number;
        }

        public void PrintIn(PrintMethod print)
        {
            for (int i = digits.Count - 1; i >= 0; i--)
            {
                if (digits[i] < 0 || digits[i] > 9)
                    throw new Exception("lol, really? You trying to insert " + digits[i] + " as digit in decimal number.");
                else
                    print(digits[i].ToString()[0]);
            }
        }
    }
}
