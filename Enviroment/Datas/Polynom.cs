﻿using System;
using System.Collections.Generic;
using Guinea.Modules;

namespace Guinea.Environment
{
    public class Polynom : List<Member>, IData
    {
        public Natural Power { get { try { return this[Count - 1].power; } catch { return new Natural(0); } } }

        public Polynom Clone()
        {
            Polynom Copy = new Polynom();
            foreach (Member member in this)
                Copy.Add(member.Clone());
            return Copy;
        }

        public Rational this[Natural power]
        {
            get
            {
                Member member = Find(x => x.power.Equals(power));
                if (member != null)
                    return member.multiplier;
                else
                    return new Rational(new Integer(signature.plus, 0), new Natural(1));
            }
            set
            {
                if (value.numerator.number.Equals(Natural.zero))
                {
                    Remove(Find(x => x.power.Equals(power)));
                    return;
                }

                if (Count == 0 || this.Power < power)
                {
                    Add(new Member(power, value));
                    return;
                }

                int i;
                for (i = Count - 1; i >= 0 && this[i].power >= power; i--) ;
                i++;

                if (this[i].power.Equals(power))
                    this[i].multiplier = value;
                else
                    Insert(i, new Member(power, value));
            }
        }

        public string GetString()
        {
            string result = "";
            bool firstEnter = false;

            if (Count == 0)
                return "0";

            for (int i = Count - 1; i >= 0; i--)
            {
                this[i].PrintIn(delegate(char c) { result += c; }, firstEnter);
                firstEnter = true;
            }

            return result;
        }

        public void PrintIn(PrintMethod print)
        {
            bool firstEnter = false;

            if (Count == 0)
            {
                print('0');
                return;
            }

            for (int i = Count - 1; i >= 0; i--)
            {
                this[i].PrintIn(print, firstEnter);
                firstEnter = true;
            }
        }

        public IData TryParse(string str)
        {
            if (str == null || str.Length == 0)
                return null;

            Polynom result = new Polynom();
            string[] elements = SplitBySigns(str);

            foreach (string element in elements)
            {
                Member member = Member.TryParse(element.Trim(' '));
                if (member != null)
                    result[member.power] = result[member.power] + member.multiplier;
                else
                    return null;
            }

            return result;
        }

        private string[] SplitBySigns(string str)
        {
            string[] splitBySpace = str.Replace("+", " + ").Replace("-", " - ").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> result = new List<string>();

            int i;
            if (splitBySpace[0] == "-" || splitBySpace[0] == "+")
                i = 0;
            else
            {
                i = 1;
                result.Add(splitBySpace[0]);
            }

            for (; i < splitBySpace.Length - 1; i = i + 2)
            {
                result.Add(splitBySpace[i] + splitBySpace[i + 1]);
            }

            if (str.LastIndexOf("+") == str.Length - 1 || str.LastIndexOf("-") == str.Length - 1)
                result.Add("");

            return result.ToArray();
        }

        public static List<Natural> GetAllPowers(params Polynom[] polynoms)
        {
            List<Natural> allPowers = new List<Natural>();
            foreach (Polynom polynom in polynoms)
            {
                foreach (Member member in polynom)
                {
                    if (!allPowers.Contains(member.power))
                        allPowers.Add(member.power);
                }
            }
            return allPowers;
        }
    }

    public class Member
    {
        public Natural power;
        public Rational multiplier;

        public Member()
        {
            power = new Natural();
            multiplier = new Rational(new Integer(signature.plus, 0), new Natural(1));
        }

        public Member(Natural power, Rational multiplier)
        {
            if (power != null && multiplier != null)
            {
                this.power = power;
                this.multiplier = multiplier;
            }
            else throw new NullReferenceException();
        }

        public Member Clone()
        {
            return new Member(power.Clone(), multiplier.Clone());
        }

        public void PrintIn(PrintMethod print, bool drawPlus)
        {
            bool printedMultiplier = false;

            if (drawPlus && multiplier.numerator.sign == signature.plus)
                print('+');

            if (!(multiplier.numerator.number.Equals(new Natural(1)) && multiplier.denominator.Equals(new Natural(1))) || power.Equals(new Natural(0)))
            {
                multiplier.PrintIn(print);
                printedMultiplier = true;
            }
            else if (multiplier.numerator.sign == signature.minus)
                print('-');

            if (!power.Equals(new Natural(0)))
            {
                if (printedMultiplier)
                    print('*');

                print('x');

                if (!power.Equals(new Natural(1)))
                {
                    print('^');
                    power.PrintIn(print);
                }
            }
        }

        public static Member TryParse(string str)
        {
            try
            {
                if (str.Contains(" "))
                    return null;

                Rational withPower0 = UI.FindData(typeof(Rational)).TryParse(str) as Rational;
                if (withPower0 != null)
                    return new Member(new Natural(0), withPower0);

                string sign = "";

                if (str.StartsWith("+x") || str.StartsWith("-x"))
                {
                    sign = str[0].ToString();
                    str = str.Substring(1);
                }

                if (str[0] == 'x')
                {
                    if (str.Length > 1)
                    {
                        if (str[1] == '^')
                            return new Member(UI.FindData(typeof(Natural)).TryParse(str.Substring(2)) as Natural,
                                              UI.FindData(typeof(Rational)).TryParse(sign + "1") as Rational);
                        else
                            return null;
                    }
                    else return new Member(new Natural(1), UI.FindData(typeof(Rational)).TryParse(sign + "1") as Rational);
                }

                if (str.Contains("*x"))
                {
                    string[] sep = new string[] { "*x^" };
                    string[] mulPowPair = str.Split(sep, StringSplitOptions.None);

                    if (mulPowPair.Length == 2)
                    {
                        if (mulPowPair[0] == "")
                            return null;

                        if (mulPowPair[1] == "")
                            return null;

                        return new Member(UI.FindData(typeof(Natural)).TryParse(mulPowPair[1]) as Natural,
                                          UI.FindData(typeof(Rational)).TryParse(mulPowPair[0]) as Rational);
                    }

                    sep[0] = "*x";
                    mulPowPair = str.Split(sep, StringSplitOptions.None);

                    if (mulPowPair.Length == 2)
                    {
                        if (mulPowPair[0] == "")
                            return null;

                        if (mulPowPair[1] != "")
                            return null;

                        return new Member(new Natural(1), UI.FindData(typeof(Rational)).TryParse(mulPowPair[0]) as Rational);
                    }
                }

                return null;
            }
            catch { return null; }
        }
    }
}
