﻿using Guinea.Modules;

namespace Guinea.Environment
{

    public class Rational : IData
    {
        public Integer numerator;
        public Natural denominator;

        public Rational() { numerator = new Integer(); denominator = new Natural(); }

        public Rational(Integer numerator, Natural denominator)
        {
            this.numerator = numerator;
            this.denominator = denominator;
        }

        public Rational Clone()
        {
            return new Rational(numerator.Clone(), denominator.Clone());
        }

        public static Rational operator +(Rational left, Rational right)
        {
            return UI.FindModule(typeof(ADD_QQ_Q)).Calculate(left, right) as Rational;
        }

        public static Rational operator -(Rational left, Rational right)
        {
            return UI.FindModule(typeof(SUB_QQ_Q)).Calculate(left, right) as Rational;
        }

        public static Rational operator *(Rational left, Rational right)
        {
            return UI.FindModule(typeof(MUL_QQ_Q)).Calculate(left, right) as Rational;
        }

        public static Rational operator /(Rational left, Rational right)
        {
            return UI.FindModule(typeof(DIV_QQ_Q)).Calculate(left, right) as Rational;
        }

        public string GetString()
        {
            if (UI.FindModule(typeof(NZER_N_B)).Calculate(denominator).GetString() == Messages.yes)
                throw new UserException("В рациональных дробях деление на ноль не определено.");

            if (denominator.Equals(new Natural(1)))
                return UI.FindModule(typeof(TRANS_Q_Z)).Calculate(this).GetString();

            return numerator.GetString() + "/" + denominator.GetString();
        }

        public void PrintIn(PrintMethod print)
        {
            if (UI.FindModule(typeof(NZER_N_B)).Calculate(denominator).GetString() == Messages.yes)
                throw new UserException("В рациональных дробях деление на ноль не определено.");

            if (denominator.Equals(new Natural(1)))
            {
                UI.FindModule(typeof(TRANS_Q_Z)).Calculate(this).PrintIn(print);
                return;
            }

            numerator.PrintIn(print);
            print('/');
            denominator.PrintIn(print);
        }

        public IData TryParse(string str)
        {
            if (str == null || str.Length == 0)
                return null;

            string[] numbers = str.Split('/');

            Integer numerator = UI.FindData(typeof(Integer)).TryParse(numbers[0]) as Integer;
            if (numerator == null)
                return null;

            if (numbers.Length == 1)
                return UI.FindModule(typeof(TRANS_Z_Q)).Calculate(numerator);
            if (numbers.Length != 2)
                return null;

            Natural denominator = UI.FindData(typeof(Natural)).TryParse(numbers[1]) as Natural;
            if (denominator == null)
                return null;
            if (UI.FindModule(typeof(NZER_N_B)).Calculate(denominator).GetString() == Messages.yes)
                throw new UserException("В рациональных дробях деление на ноль не определено.");

            return new Rational(numerator, denominator);
        }
    }
}
