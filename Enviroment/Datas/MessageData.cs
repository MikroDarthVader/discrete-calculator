﻿using System;

namespace Guinea.Environment
{
    public class MessageData : IData
    {
        string message;

        public MessageData()
        {
            message = "";
        }

        public MessageData(string message)
        {
            this.message = message;
        }
        
        public MessageData Clone()
        {
            return new MessageData(message);
        }

        public string GetString()
        {
            return message;
        }

        public void PrintIn(PrintMethod print)
        {
            foreach (char c in message)
                print(c);
        }

        public IData TryParse(string str)
        {
            return null;
        }
    }
}
