﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CodeDom.Compiler;
using System.Text.RegularExpressions;
using FastColoredTextBoxNS;
using System.Diagnostics;

namespace Guinea.Environment
{
    class Variables : IData
    {
        public static FastConsole Console;
        private static string validVariableRegex = @"[\p{L}]\w*";
        private static string beforeEqualRegex = @"(^\s*" + validVariableRegex + @"\s*=)";
        private static string regex = "";
        public static string GetRegex() { return regex; }
        public static string GetBeforeEqualRegex() { return beforeEqualRegex; }

        public Dictionary<string, IData> variables;
        public List<string> VariablesNames;

        public Variables()
        {
            variables = new Dictionary<string, IData>();
            VariablesNames = new List<string>();
        }

        public string GetString()
        {
            return null;
        }

        public void PrintIn(PrintMethod print) { }

        public IData TryParse(string str)
        {
            if (str.Contains('='))
                return TryWriteToVariable(str);
            else
                return TryGetVariable(str);
        }

        private IData TryGetVariable(string str)
        {
            if (variables.ContainsKey(str))
                return variables[str];
            else
                return null;
        }

        private IData TryWriteToVariable(string str)
        {
            string[] varValPair = str.Split('=');

            if (varValPair.Length != 2)
                throw new CanNotParseException();

            varValPair[0] = varValPair[0].Trim(' ');
            varValPair[1] = varValPair[1].Trim(' ');

            if (varValPair[0] == "x")
                throw new UserException("\"x\" - параметр многочлена, и не может быть записан как переменная");

            if (!Regex.IsMatch(varValPair[0], validVariableRegex))
                throw new UserException("Имя переменной не может начинаться с цифры, или содержать специальные символы");

            IData result = UI.TryParseAsData(varValPair[1]);

            if (variables.ContainsKey(varValPair[0]))
                variables[varValPair[0]] = result;
            else
            {
                if (variables.Count > 0)
                    regex = regex.Substring(0, regex.Length - 10) + "|" + varValPair[0] + @")([^\w]|$)";
                else
                    regex = @"(\n|\r|^|\(|=|;)\s*(" + varValPair[0] + @")([^\w]|$)";

                variables.Add(varValPair[0], result);
                VariablesNames.Add(varValPair[0]);
            }
            StringBuilder val = new StringBuilder();
            result.PrintIn(delegate (char c) { val.Append(c); });
            Console.NewVariableAction(varValPair[0], val.ToString());

            return result;
        }
    }
}
