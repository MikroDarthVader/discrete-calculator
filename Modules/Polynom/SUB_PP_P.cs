﻿using Guinea.Environment;
using System;
using System.Collections.Generic;

namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Вычитание многочленов)
    /// </summary>
    class SUB_PP_P : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Уменьшаемый многочлен", "Вычитаемый многочлен" };

        public string PrintableName { get { return "Вычитание многочленов"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom), typeof(Polynom) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly1 = (data[0] as Polynom).Clone();
            Polynom poly2 = data[1] as Polynom;
            
            //Substaction of the corresponding elements //Вычитание соотв. коэффициентов из большего многочлена
            foreach (Member i in poly2)
            {
                poly1[i.power] -= i.multiplier;
            }
            return poly1;
        }
    }
}
