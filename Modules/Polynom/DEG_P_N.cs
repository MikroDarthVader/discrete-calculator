﻿using Guinea.Environment;
using System;


namespace Guinea.Modules //namespace must be same as here
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Степень многочлена)
    /// </summary>
    public class DEG_P_N : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Многочлен" };

        public string PrintableName { get { return "Степень многочлена"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            return (data[0] as Polynom).Power.Clone();
        }
    }
}