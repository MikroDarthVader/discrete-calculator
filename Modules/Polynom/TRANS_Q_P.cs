﻿using Guinea.Environment;
using System;


namespace Guinea.Modules
{
    /// <summary>
    /// Коровин Михаил 
    /// (Преобразование рациональной дроби в многочлен нулевой степени)
    /// </summary>
    class TRANS_Q_P : IModule
    {
        public bool VisibleInTree { get; } = false;
        public string[] Hints { get; } = new string[] { "Рациональная дробь"};

        public string PrintableName { get { return "Преобразование рациональной дроби в многочлен нулевой степени"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly = new Polynom();
            poly[Natural.zero] = data[0] as Rational;
            return poly;
        }
    }
}
