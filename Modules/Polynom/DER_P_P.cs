﻿using Guinea.Environment;
using System;
using System.Collections.Generic;

namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Производная многочлена)
    /// </summary>
    class DER_P_P : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string[] Hints { get; } = new string[] { "Изначальный многочлен" };

        public string PrintableName { get { return "Производная многочлена"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly = (data[0] as Polynom).Clone();
            Polynom result = new Polynom();

            foreach (Member i in poly)
            {
                //Multiplying i by rational at [i] and placing it into i-1 //умножение i коэффициента на i и переставление его в i-1
                result[i.power - new Natural(1)] = i.multiplier * new Rational(new Integer(signature.plus, i.power), new Natural(1));
            }

            return result;
        }
    }
}
