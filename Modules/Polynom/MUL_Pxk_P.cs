﻿using System;
using System.Collections.Generic;
using Guinea.Environment;

namespace Guinea.Modules //namespace must be same as here
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Умножение многочлена на x^k)
    /// </summary>
    public class MUL_Pxk_P : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Умножаемый многочлен", "Натуральное число k для умножения многочлена на x^k" };

        public string PrintableName { get { return "Умножение многочлена на x^k"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom), typeof(Natural) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom result = (data[0] as Polynom).Clone();
            Natural k = data[1] as Natural;
            //Прибавление к степени каждого x натурального числа k
            foreach (Member member in result)
            {
                member.power += k;
            }

            return result;
        }
    }
}
