﻿using Guinea.Environment;
using System;


namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Преобразование многочлена — кратные корни в простые)
    /// </summary>
    class NMR_P_P : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Многочлен для преобразования" };

        public string PrintableName { get { return "Преобразование многочлена — кратные корни в простые"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom)};

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly1 = (data[0] as Polynom).Clone();

            //Used modules
            DIV_PP_P div_pp_p = UI.FindModule(typeof(DIV_PP_P)) as DIV_PP_P;
            FAC_P_Q fac_p_q = UI.FindModule(typeof(FAC_P_Q)) as FAC_P_Q;
            GCF_PP_P gcf_pp_p = UI.FindModule(typeof(GCF_PP_P)) as GCF_PP_P;
            MUL_PQ_P mul_pq_p = UI.FindModule(typeof(MUL_PQ_P)) as MUL_PQ_P;
            DER_P_P der_p_p = UI.FindModule(typeof(DER_P_P)) as DER_P_P;
            LED_P_Q led_p_q = UI.FindModule(typeof(LED_P_Q)) as LED_P_Q;
            //
            //Return poly if it == 0 //Вернуть полином если он нулевой
            if (poly1[Natural.zero].numerator.number.Equals(Natural.zero) && poly1.Power == Natural.zero) return poly1;
            //Computation of the derivative //вычислить производную
            Polynom poly1_der = der_p_p.Calculate(poly1) as Polynom;
            //Computation of the GCF//вычислить нод производной и изначального полинома
            Polynom poly1_gcf = gcf_pp_p.Calculate(poly1, poly1_der) as Polynom;
            
            //поделить полином на нод полинома и его производной
            Polynom result = div_pp_p.Calculate(poly1, poly1_gcf) as Polynom;
            Rational fac = fac_p_q.Calculate(result) as Rational;
            //Reduction of a polynomial for a more convenient form //сократить для более удобного вида
            Polynom fac_p = new Polynom();
            fac_p[Natural.zero] = fac;
            result = div_pp_p.Calculate(result, fac_p) as Polynom;
            if ((led_p_q.Calculate(result) as Rational).numerator.sign == signature.minus) result = mul_pq_p.Calculate(result, new Rational(new Integer(signature.minus, new Natural(1)), new Natural(1))) as Polynom;
            return result;
        }
    }
}
