﻿using Guinea.Environment;
using System;


namespace Guinea.Modules //namespace must be same as here
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Старший коэффициент многочлена)
    /// </summary>
    public class LED_P_Q : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Многочлен" };

        public string PrintableName { get { return "Старший коэффициент многочлена"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom)};

        public Type ReturnType { get { return typeof(Rational); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly = data[0] as Polynom;

            return poly[poly.Power].Clone();
        }
    }
}