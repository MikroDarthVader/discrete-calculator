﻿using Guinea.Environment;
using System;

namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Остаток от деления многочлена на многочлен при делении с остатком)
    /// </summary>
    class MOD_PP_P : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Делимый многочлен", "Многочлен-делитель" };

        public string PrintableName { get { return "Остаток от деления многочлена на многочлен при делении с остатком"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom), typeof(Polynom) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly1 = (data[0] as Polynom).Clone();
            Polynom poly2 = (data[1] as Polynom).Clone();

            Polynom result = new Polynom();
            if (poly1.Power < poly2.Power) return poly1;
            //Used modules
            MUL_Pxk_P mul_pxk_p = UI.FindModule(typeof(MUL_Pxk_P)) as MUL_Pxk_P;
            MUL_PQ_P mul_pq_p = UI.FindModule(typeof(MUL_PQ_P)) as MUL_PQ_P;
            SUB_PP_P sub_pp_p = UI.FindModule(typeof(SUB_PP_P)) as SUB_PP_P;
            LED_P_Q led_p_q = UI.FindModule(typeof(LED_P_Q)) as LED_P_Q;

            Polynom cur;
            Rational mn;
            //While poly1 != 0 and Power of poly1 >= poly2 //Пока poly1 не обнулится и степень poly1 >= степени poly2
            while (poly1.Power >= poly2.Power && !(poly1.Power.Equals(Natural.zero) && poly1[Natural.zero].numerator.number.Equals(Natural.zero)))
            {
                mn = ((led_p_q.Calculate(poly1) as Rational) / (led_p_q.Calculate(poly2) as Rational));
                cur = mul_pq_p.Calculate(poly2, mn) as Polynom;
                cur = mul_pxk_p.Calculate(cur, poly1.Power - poly2.Power) as Polynom;
                //adding to the result //добавление к результату текущего
                //substracting //вычитание из исходного
                poly1 = sub_pp_p.Calculate(poly1, cur) as Polynom;
            }

            return poly1;
        }
    }
}

