﻿using Guinea.Environment;
using System;
using System.Collections.Generic;

namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Умножение многочлена на рациональное число)
    /// </summary>
    class MUL_PQ_P : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Умножаемый многочлен", "Рациональный множитель" };

        public string PrintableName { get { return "Умножение многочлена на рациональное число"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom), typeof(Rational) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom result = (data[0] as Polynom).Clone();
            Rational rational = data[1] as Rational;

            //Multiplying every element //Умножение каждого коэффициента
            foreach (Member member in result)
            {
                member.multiplier *= rational;
            }
            return result;
        }
    }
}
