﻿using Guinea.Environment;
using System;


namespace Guinea.Modules //namespace must be same as here
{
    /// <summary>
    /// Коровин Михаил 
    /// (Преобразование многочлена нулевой степени в рациональную дробь)
    /// </summary>
    public class TRANS_P_Q : IModule
    {
        public bool VisibleInTree { get; } = false;
        public string[] Hints { get; } = new string[] { "Полином" };

        public string PrintableName { get { return "Преобразование многочлена нулевой степени в рациональную дробь"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom input = data[0] as Polynom;
            if (input.Power.Equals(Natural.zero))
                return input[Natural.zero];
            else
                throw new UserException("Невозможно преобразовать многочлен ненулевой степени в рациональное");
        }
    }
}