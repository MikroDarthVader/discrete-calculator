﻿using Guinea.Environment;
using System;
using System.Collections.Generic;

namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Сложение многочленов)
    /// </summary>
    class ADD_PP_P : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string[] Hints { get; } = new string[] { "Первое слагаемое-многочлен", "Второе слагаемое-многочлен" };

        public string PrintableName { get { return "Сложение многочленов"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom), typeof(Polynom) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly1 = (data[0] as Polynom).Clone();
            Polynom poly2 = (data[1] as Polynom).Clone();

            if (poly2.Count > poly1.Count)
            {
                Polynom temp = poly2;
                poly2 = poly1;
                poly1 = temp;
            }

            //Addition of the corresponding elements //Сложение соотв. коэффициентов
            foreach (Member i in poly2)
            {
                poly1[i.power] += i.multiplier;
            }
            return poly1;
        }
    }
}
