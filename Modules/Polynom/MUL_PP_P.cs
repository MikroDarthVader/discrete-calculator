﻿using System;
using System.Collections.Generic;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Умножение многочленов)
    /// </summary>
    class MUL_PP_P : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Множитель-многочлен", "Множитель-многочлен" };

        public string PrintableName { get { return "Умножение многочленов"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom), typeof(Polynom) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly1 = data[0] as Polynom;
            Polynom poly2 = data[1] as Polynom;
            Polynom result = new Polynom();
            Polynom currentMultiply;
            //Used modules
            MUL_PQ_P mul_pq_q = UI.FindModule(typeof(MUL_PQ_P)) as MUL_PQ_P;
            MUL_Pxk_P mul_pxk_p = UI.FindModule(typeof(MUL_Pxk_P)) as MUL_Pxk_P;
            ADD_PP_P add_pp_p = UI.FindModule(typeof(ADD_PP_P)) as ADD_PP_P;
            //
            foreach (Member i in poly2)
            {
                currentMultiply = mul_pq_q.Calculate(poly1, i.multiplier) as Polynom; //Multipluing cur to poly2[i]//Умножить cur на текущий коэффициент
                currentMultiply = mul_pxk_p.Calculate(currentMultiply, i.power) as Polynom; //Shift of a polynomial right by i//Домножить на x^i
                result = add_pp_p.Calculate(currentMultiply, result) as Polynom; //Addition of the current with the result//Добавить cur к результату
            }
            return result;
        }
    }
}

