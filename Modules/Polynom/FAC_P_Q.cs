﻿using Guinea.Environment;
using System;
using System.Collections.Generic;

namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (Вынесение из многочлена НОК знаменателей коэффициентов и НОД числителей)
    /// </summary>
    class FAC_P_Q : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Изначальный многочлен" };

        public string PrintableName { get { return "Вынесение из многочлена НОК знаменателей коэффициентов и НОД числителей"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom) };

        public Type ReturnType { get { return typeof(Rational); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly = (data[0] as Polynom).Clone();
            Natural NOK = new Natural(1);
            Natural NOD = new Natural(1);
            //Used modules

            LCM_NN_N lcm_nn_n = UI.FindModule(typeof(LCM_NN_N)) as LCM_NN_N;
            GCF_NN_N gcf_nn_n = UI.FindModule(typeof(GCF_NN_N)) as GCF_NN_N;
            TRANS_N_Z trans_n_z = UI.FindModule(typeof(TRANS_N_Z)) as TRANS_N_Z;
            //
            NOD = poly[Natural.zero].numerator.number;
            NOK = poly[Natural.zero].denominator;
            foreach (Member i in poly)
            {
                //Calculating LCM //Вычисление НОК знаменателя
                NOK = lcm_nn_n.Calculate(NOK, i.multiplier.denominator) as Natural;
                //Calculating GCF //Вычисление НОД числителя
                NOD = gcf_nn_n.Calculate(NOD, i.multiplier.numerator.number as Natural) as Natural;
            }

            //Translate to Integer //Перевод в знаковое числителя
            Integer numerator = trans_n_z.Calculate(NOD) as Integer;
            return new Rational(numerator, NOK);
        }
    }
}

