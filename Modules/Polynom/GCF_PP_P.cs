﻿using Guinea.Environment;
using System;

namespace Guinea.Modules
{
    /// <summary>
    /// Борисенко Алексей 
    /// (НОД многочленов)
    /// </summary>
    class GCF_PP_P : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Первый многочлен", "Второй многочлен" };

        public string PrintableName { get { return "НОД многочленов"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.polynomial; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Polynom), typeof(Polynom) };

        public Type ReturnType { get { return typeof(Polynom); } }

        public IData Calculate(params IData[] data)
        {
            Polynom poly1 = (data[0] as Polynom).Clone();
            Polynom poly2 = (data[1] as Polynom).Clone();
            Polynom cur = new Polynom();

            //Used modules
            DEG_P_N deg_p_n = UI.FindModule(typeof(DEG_P_N)) as DEG_P_N;
            MOD_PP_P mod_pp_p = UI.FindModule(typeof(MOD_PP_P)) as MOD_PP_P;
            //The Euclidean algorithm is used(sequential division with remainder)//Используется алгоритм Евклида, последовательное получение остатка от деления до обнуления poly2
            while (!(poly2.Power.Equals(Natural.zero) && poly2[Natural.zero].numerator.number.Equals(Natural.zero)))
            {
                cur = mod_pp_p.Calculate(poly1, poly2) as Polynom;
                poly1 = poly2.Clone();
                poly2 = cur.Clone();
            }

            return poly1;
        }
    }
}
