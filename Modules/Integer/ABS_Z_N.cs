﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-1
    /// (Aбсолютная величина числа)
    /// </summary>
    public class ABS_Z_N : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "целое число" };

        public string PrintableName { get { return "Aбсолютная величина числа"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer)};

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            return (data[0] as Integer).number; // возвращение числа без знака
        }
    }
}
