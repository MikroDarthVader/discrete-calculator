﻿using Guinea.Environment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guinea.Modules
{
    /// <summary>
    /// Казанцев Дмитрий; Module Z-1
    /// (Возведение целого числа в натуральную степень)
    /// </summary>
    public class POW_Z_N : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Число, возводимое в целую степень", "натуральная степень" };

        public string PrintableName { get { return "Возведение целого числа в натуральную степень"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer), typeof(Natural) };

        public Type ReturnType { get { return typeof(Integer); } }

        public IData Calculate(params IData[] data)
        {
            Integer number = (data[0] as Integer).Clone();
            Integer begin = data[0] as Integer;
            Natural power = data[1] as Natural;

            MUL_ZZ_Z MUL_zz_z = UI.FindModule(typeof(MUL_ZZ_Z)) as MUL_ZZ_Z;
            POW_N_N POW_n_n = UI.FindModule(typeof(POW_N_N)) as POW_N_N;
            MOD_NN_N MOD_nn_n = UI.FindModule(typeof(MOD_NN_N)) as MOD_NN_N;


            if (power.Equals(Natural.zero) || number.Equals(new Integer(signature.plus, 1)))
            {
                return new Integer(signature.plus, 1);
            }
            number = new Integer(signature.plus,  POW_n_n.Calculate(number.number, power)as Natural);

            if ( (MOD_nn_n.Calculate(power, new Natural(2) ) as Natural).Equals(new Natural(1)) && (begin.sign == signature.minus))
            {
                number.sign = signature.minus;
            }
            
            return number;
        }
    }
}
