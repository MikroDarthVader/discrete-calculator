﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-6
    /// (Cложение целых чисел)
    /// </summary>
    public class ADD_ZZ_Z : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Первое слагаемое-целое", "Второе слагаемое-целое" };

        public string PrintableName { get { return "Cложение целых чисел"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer), typeof(Integer) };

        public Type ReturnType { get { return typeof(Integer); } }

        public IData Calculate(params IData[] data)
        {
            TRANS_N_Z Trans_N_Z = UI.FindModule(typeof(TRANS_N_Z)) as TRANS_N_Z;
            ADD_NN_N Add_NN_N = UI.FindModule(typeof(ADD_NN_N)) as ADD_NN_N;
            MUL_ZM_Z Mul_ZM_Z = UI.FindModule(typeof(MUL_ZM_Z)) as MUL_ZM_Z;
            SUB_NN_N Sub_NN_N = UI.FindModule(typeof(SUB_NN_N)) as SUB_NN_N;
            COM_NN_D Com_NN_D = UI.FindModule(typeof(COM_NN_D)) as COM_NN_D;

            Integer num1 = data[0] as Integer;
            Integer num2 = data[1] as Integer;
            Integer result;

            if (num1.number < num2.number) //нахождение большего
            {
                Integer temp = num1;
                num1 = num2;
                num2 = temp;
            }

            if (num1.sign == num2.sign)
                result = Trans_N_Z.Calculate(num1.number + num2.number) as Integer; //одинаковые знаки => складываем
            else
                result = Trans_N_Z.Calculate(num1.number - num2.number) as Integer; //разные => вычитаем из большего меньшее 

            if (num1.sign == signature.minus)
                result = Mul_ZM_Z.Calculate(result) as Integer; // если большее с "-", домножаем результат на -1

            return result;
        }
    }
}
