﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-2
    /// (Определение положительности числа)
    /// </summary>
    public class POZ_Z_D : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "целое" };

        public string PrintableName { get { return "Определение положительности числа"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer) };

        public Type ReturnType { get { return typeof(MessageData); } }

        public IData Calculate(params IData[] data)
        {
            Integer num1 = data[0] as Integer;

            if (num1.number[0] == 0 && num1.Length == 1)
                return new MessageData(Messages.zero);
            if (num1.sign == signature.plus)
                return new MessageData(Messages.positive);

            return new MessageData(Messages.negative);
        }
    }
}
