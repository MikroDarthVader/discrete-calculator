﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-3
    /// (Умножение целого на (-1))
    /// </summary>
    public class MUL_ZM_Z : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "число-целое" };

        public string PrintableName { get { return "Умножение целого на (-1)"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer) };

        public Type ReturnType { get { return typeof(Integer); } }

        public IData Calculate(params IData[] data)
        {
            Integer num1 = (data[0] as Integer).Clone();
            //смена знака
            if (num1.sign == signature.minus)
                num1.sign = signature.plus;
            else
                num1.sign = signature.minus;

            return num1;
        }
    }
}
