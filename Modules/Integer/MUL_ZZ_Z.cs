﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-8
    /// (Умножение целых чисел)
    /// </summary>
    public class MUL_ZZ_Z : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Первый множитель-целое", "Второй множитель-целое" };

        public string PrintableName { get { return "Умножение целых чисел"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer), typeof(Integer) };

        public Type ReturnType { get { return typeof(Integer); } }

        public IData Calculate(params IData[] data)
        {
            TRANS_N_Z Trans_N_Z = UI.FindModule(typeof(TRANS_N_Z)) as TRANS_N_Z;
            MUL_NN_N Mul_NN_N = UI.FindModule(typeof(MUL_NN_N)) as MUL_NN_N;
            ABS_Z_N Abs_Z_N = UI.FindModule(typeof(ABS_Z_N)) as ABS_Z_N;
            MUL_ZM_Z Mul_ZM_Z = UI.FindModule(typeof(MUL_ZM_Z)) as MUL_ZM_Z;

            Integer num1 = data[0] as Integer;
            Integer num2 = data[1] as Integer;

            Integer result = Trans_N_Z.Calculate(num1.number * num2.number) as Integer; // умножение и трансформация в натуральное

            if (num1.sign != num2.sign)
                result = Mul_ZM_Z.Calculate(result) as Integer; // при разных знаках результат отрицательный

            return result;
        }
    }
}
