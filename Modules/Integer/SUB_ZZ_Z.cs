﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-7
    /// (Вычитание из левого числа правого)
    /// </summary>
    public class SUB_ZZ_Z : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Уменьшаемое-целое", "Вычитаемое-целое" };

        public string PrintableName { get { return "Вычитание из левого числа правого"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer), typeof(Integer) };

        public Type ReturnType { get { return typeof(Integer); } }

        public IData Calculate(params IData[] data)
        {
            MUL_ZM_Z Mul_ZM_Z = UI.FindModule(typeof(MUL_ZM_Z)) as MUL_ZM_Z;
            ADD_ZZ_Z Add_ZZ_Z = UI.FindModule(typeof(ADD_ZZ_Z)) as ADD_ZZ_Z;
            
            Integer num1 = data[0] as Integer;
            Integer num2 = data[1] as Integer;
            num2 = Mul_ZM_Z.Calculate(num2) as Integer; //смена знака у вычитаемого
            return num1 + num2; //сумма
        }
    }
}
