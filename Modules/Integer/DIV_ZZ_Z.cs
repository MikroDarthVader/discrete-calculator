﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-9
    /// (Частное от деления большего целого на меньшее или равное натуральное)
    /// </summary>
    class DIV_ZZ_Z : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "делимое-целое", "делитель-натуральное" };

        public string PrintableName { get { return "Частное от деления большего целого на меньшее или равное натуральное"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer), typeof(Natural) };

        public Type ReturnType { get { return typeof(Integer); } }

        public IData Calculate(params IData[] data)
        {
            MUL_ZM_Z Mul_ZM_Z = UI.FindModule(typeof(MUL_ZM_Z)) as MUL_ZM_Z;
            DIV_NN_N Div_NN_N = UI.FindModule(typeof(DIV_NN_N)) as DIV_NN_N;
            ABS_Z_N Abs_Z_N = UI.FindModule(typeof(ABS_Z_N)) as ABS_Z_N;
            TRANS_N_Z Trans_N_Z = UI.FindModule(typeof(TRANS_N_Z)) as TRANS_N_Z;
            ADD_1N_N Add_1N_N = UI.FindModule(typeof(ADD_1N_N)) as ADD_1N_N;
            Integer num1 = data[0] as Integer;
            Natural num2 = data[1] as Natural;
            Integer result = Trans_N_Z.Calculate(num1.number / num2) as Integer; // деление и преобразование результата в целое

            if (num1.sign == signature.minus)
            {
                if (!((num1.number / num2) * num2).Equals(num1.number))
                {
                    result.number++; // add_1n_n 
                }
            result = Mul_ZM_Z.Calculate(result) as Integer;    
            }
            return result;
        }
    }
}
