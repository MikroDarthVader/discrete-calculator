﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-5
    /// (Преобразование целого неотрицательного в натуральное)
    /// </summary>
    public class TRANS_Z_N : IModule
    {
        public bool VisibleInTree { get; } = false;
        public string[] Hints { get; } = new string[] { "Целое неотрицательное число"};

        public string PrintableName { get { return "Преобразование целого неотрицательного в натуральное"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            POZ_Z_D Poz_Z_D = UI.FindModule(typeof(POZ_Z_D)) as POZ_Z_D;
            Integer num = data[0] as Integer;

            if (Poz_Z_D.Calculate(num).GetString() == Messages.negative) //проверка на неотрицательность
                throw new UserException("Невозможно преобразовать отрицательное целое в натуральное");

            return num.number; //вывод натурального
        }
    }
}
