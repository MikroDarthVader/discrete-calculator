﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-4
    /// (Преобразование натурального в целое)
    /// </summary>
    public class TRANS_N_Z : IModule
    {
        public bool VisibleInTree { get; } = false;
        public string[] Hints { get; } = new string[] { "Число-натуральное" };

        public string PrintableName { get { return "Преобразование натурального в целое"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural) };

        public Type ReturnType { get { return typeof(Integer); } }

        public IData Calculate(params IData[] data)
        {
            return new Integer(signature.plus, (data[0] as Natural)); // добавлеие знака
        }
    }
}
