﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Городов Леонид, Михайлов Никита; Module Z-10
    /// (Остаток от деления большего целого на натуральное)
    /// </summary>
    class MOD_ZZ_Z : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "делимое-целое", "делитель-натуральное" };

        public string PrintableName { get { return "Остаток от деления целого на натуральное"; } }

        public moduleType Type { get { return moduleType.integer; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer), typeof(Natural) };

        public Type ReturnType { get { return typeof(Integer); } }

        public IData Calculate(params IData[] data)
        {
            MUL_ZZ_Z Mul_ZZ_Z = UI.FindModule(typeof(MUL_ZZ_Z)) as MUL_ZZ_Z;
            DIV_ZZ_Z Div_ZZ_Z = UI.FindModule(typeof(DIV_ZZ_Z)) as DIV_ZZ_Z;
            SUB_ZZ_Z Sub_ZZ_Z = UI.FindModule(typeof(SUB_ZZ_Z)) as SUB_ZZ_Z;
            TRANS_N_Z Trans_N_Z = UI.FindModule(typeof(TRANS_N_Z)) as TRANS_N_Z;
            POZ_Z_D Poz_Z_D = UI.FindModule(typeof(POZ_Z_D)) as POZ_Z_D;
            ADD_ZZ_Z Add_ZZ_Z = UI.FindModule(typeof(ADD_ZZ_Z)) as ADD_ZZ_Z;
            MUL_ZM_Z Mul_ZM_Z = UI.FindModule(typeof(MUL_ZM_Z)) as MUL_ZM_Z;
            Integer num1 = data[0] as Integer;
            Natural nat = data[1] as Natural;
            Integer num2 = Trans_N_Z.Calculate(nat) as Integer; // преобразование натурального в целое
            Integer result = (num1 - ((num1 / nat) * num2)) as Integer; //поиск остатка 
            return result;
        }
    }
}
