﻿using Guinea.Environment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guinea.Modules
{
    /// <summary>
    /// Казанцев Дмитрий; Module Z-1
    /// (Возведение целого числа в натуральную степень)
    /// </summary>
    class POW_N_N : IModule
    {
        public bool VisibleInTree { get; } = true;
        public string[] Hints { get; } = new string[] { "Число, возводимое в натуральную степень", "натуральная степень" };

        public string PrintableName { get { return "Возведение натурального числа в натуральную степень"; } }

        public moduleType Type { get { return moduleType.natural; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural number = (data[0] as Natural).Clone();
            Natural begin = data[0] as Natural;
            int power;
            MUL_NN_N MUL_nn_n = UI.FindModule(typeof(MUL_NN_N)) as MUL_NN_N;
            try
            {
                power = int.Parse(data[1].GetString());
            }
            catch
            {
                throw new UserException("Степень слижком велика");
            }
            if (power == 0 || number.Equals(new Natural(1)))
            {
                return new Natural(1);
            }

            for (int i = 1; i < power; i++)
            {
                number = number * begin;
            }

            return number;
        }
    }
}
