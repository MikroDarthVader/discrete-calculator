﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-1
    /// (Сравнение левого числа с правым)
    /// </summary>
    public class COM_NN_D : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Сравнение левого числа с правым"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Первое сравниваемое число", "Второе сравниваемое число"};

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(MessageData); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = data[0] as Natural;
            Natural num2 = data[1] as Natural;

            if (num1.Length > num2.Length)
            {
                return new MessageData(Messages.bigger);
            }
            else if (num1.Length < num2.Length)
            {
                return new MessageData(Messages.less);
            }

            for (int i = num1.Length - 1; i >= 0; i--)
            {
                if (num1[i] > num2[i])
                {
                    return new MessageData(Messages.bigger);
                }
                else if (num1[i] < num2[i])
                {
                    return new MessageData(Messages.less);
                }
            }
            return new MessageData(Messages.equal);
        }
    }
}
