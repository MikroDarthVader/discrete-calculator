﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-8
    /// (Умножение натуральных чисел)
    /// </summary>
    public class MUL_NN_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Умножение натуральных чисел"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Первый множитель", "Второй множитель" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = data[0] as Natural;
            Natural num2 = data[1] as Natural;
            ADD_NN_N Add_NN_N = UI.FindModule(typeof(ADD_NN_N)) as ADD_NN_N;
            MUL_Nk_N Mul_Nk_N = UI.FindModule(typeof(MUL_Nk_N)) as MUL_Nk_N;
            MUL_ND_N Mul_ND_N = UI.FindModule(typeof(MUL_ND_N)) as MUL_ND_N;
            ADD_1N_N Add_1N_N = UI.FindModule(typeof(ADD_1N_N)) as ADD_1N_N;

            Natural temp = new Natural();
            Natural digit = new Natural(0);

            if (num1.Length < num2.Length)
            {
                temp = num1;
                num1 = num2;
                num2 = temp;
            }

            Natural result = new Natural(0);

            for (int i = 0; i < num2.Length; i++)
            {
                result = (Add_NN_N.Calculate(result, (Mul_Nk_N.Calculate((Mul_ND_N.Calculate(num1, new Natural(num2[i]))), digit))) as Natural);
                digit = Add_1N_N.Calculate(digit) as Natural;
            }

            for (int i = result.Length - 1; result[i] == 0 && result.Length != 1; i--)
                result.Remove(i);

            return result;
        }
    }
}