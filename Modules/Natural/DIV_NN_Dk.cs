﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-10
    /// (Вычисление частного от деления натуральных с остатком, и его округление вниз до максимального разряда)
    /// </summary>
    public class DIV_NN_Dk : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Вычисление частного от деления натуральных с остатком, и его округление вниз до максимального разряда"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Делимое", "Делитель" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = data[0] as Natural;
            Natural num2 = data[1] as Natural;
            Natural result;
            Natural num1_new = new Natural();
            Natural power = new Natural(0);

            ADD_1N_N Add_1N_N = UI.FindModule(typeof(ADD_1N_N)) as ADD_1N_N;
            ADD_NN_N Add_NN_N = UI.FindModule(typeof(ADD_NN_N)) as ADD_NN_N;
            MUL_Nk_N Mul_Nk_N = UI.FindModule(typeof(MUL_Nk_N)) as MUL_Nk_N;
            COM_NN_D Com_NN_D = UI.FindModule(typeof(COM_NN_D)) as COM_NN_D;

            if (Com_NN_D.Calculate(num2, num1).GetString() == Messages.bigger)
                return new Natural(0);

            int i;
            for (i = 0; i < num2.Length; i++)
            {
                num1_new.Insert(0, num1[num1.Length - 1 - i]);
            }

            if (Com_NN_D.Calculate(num1_new, num2).GetString() == Messages.less)
            {
                num1_new.Insert(0, num1[num1.Length - 1 - i]);
            }

            i = 0;
            result = num2;
            while (Com_NN_D.Calculate(result, num1_new).GetString() == Messages.less || Com_NN_D.Calculate(result, num1_new).GetString() == Messages.equal)
            {
                result = Add_NN_N.Calculate(result, num2) as Natural;
                i++;
            }

            result = Add_NN_N.Calculate(result, num2) as Natural;
            while (Com_NN_D.Calculate(result, num1).GetString() == Messages.less || Com_NN_D.Calculate(result, num1).GetString() == Messages.equal)
            {
                result = Mul_Nk_N.Calculate(result, new Natural(1)) as Natural;
                power = Add_1N_N.Calculate(power) as Natural;
            }
            result = Mul_Nk_N.Calculate(new Natural((sbyte)i), power) as Natural;

            return result;
        }
    }
}