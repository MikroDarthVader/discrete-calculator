﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-4
    /// (Сложение двух натуральных чисел)
    /// </summary>
    public class ADD_NN_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Сложение двух натуральных чисел"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Первое слагаемое", "Второе слагаемое" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = data[0] as Natural;
            Natural num2 = data[1] as Natural;

            Natural result = new Natural();

            sbyte carrier = 0;
            int i;
            for (i = 0; i < num1.Length || i < num2.Length; i++)
            {
                if (i < num1.Length && i < num2.Length)
                {
                    result.Insert(i, (sbyte)((num1[i] + num2[i] + carrier) % 10));
                    carrier = (sbyte)((num1[i] + num2[i] + carrier) / 10);
                }
                else
                {
                    result.Insert(i, (sbyte)((carrier + (num1.Length > num2.Length ? num1[i] : num2[i])) % 10));
                    carrier = (sbyte)((carrier + (num1.Length > num2.Length ? num1[i] : num2[i])) / 10);
                }
            }

            if (carrier != 0)
                result.Insert(i, carrier);

            return result;
        }
    }
}
