﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-6
    /// (Умножение натурального числа на цифру)
    /// </summary>
    public class MUL_ND_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Умножение натурального числа на цифру"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Первый множитель", "Второй множитель (Цифра от 0 до 9)" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num = data[0] as Natural;
            Natural digit = data[1] as Natural;

            if (digit.Length > 1)
                throw new UserException("Цифра не может быть больше 9, или меньше 0");

            Natural result = new Natural();

            int i;
            sbyte cnt = 0;
            for (i = 0; i < num.Length; i++)
            {
                result.Insert(i, (sbyte)((num[i] * digit[0] + cnt) % 10));

                cnt = (sbyte)((num[i] * digit[0] + cnt) / 10);
            }

            if (cnt != 0)
                result.Insert(i, cnt);

            return result;
        }
    }
}