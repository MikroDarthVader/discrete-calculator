﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-7
    /// (Умножение натурального числа на 10^k)
    /// </summary>
    public class MUL_Nk_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Умножение натурального числа на 10^k"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Число", "Число 'k', которое будет в степени '10', умноженного на введённое число" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num = data[0] as Natural;
            int power;
            try
            {
                power = int.Parse(data[1].GetString());
            }
            catch
            {
                throw new UserException("Значение степени k слишком велико.");
            }

            Natural result = new Natural(new sbyte[num.Length + power]);

            int i;
            for (i = 0; i < num.Length; i++)
                result[result.Length - num.Length + i] = num[i];
            return result;
        }
    }
}
