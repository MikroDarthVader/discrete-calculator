﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-2
    /// (Проверка на ноль)
    /// </summary>
    public class NZER_N_B : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Проверка на ноль"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Число для проверки на ноль" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural) };

        public Type ReturnType { get { return typeof(MessageData); } }

        public IData Calculate(params IData[] data)
        {
            Natural num = data[0] as Natural;

            if (num[0] == 0 && num.Length == 1)
                return new MessageData(Messages.yes);
            else
                return new MessageData(Messages.no);
        }
    }
}
