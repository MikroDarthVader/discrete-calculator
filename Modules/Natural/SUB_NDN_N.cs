﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-9
    /// (Вычитание из натурального другого натурального, умноженного на цифру для случая с неотрицательным результатом)
    /// </summary>
    public class SUB_NDN_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Вычитание из натурального другого натурального, умноженного на цифру для случая с неотрицательным результатом"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Уменьшаемое число", "Вычитаемое число", "Цифра, на которую умножается вычитаемое" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {

            SUB_NN_N Sub_NN_N = UI.FindModule(typeof(SUB_NN_N)) as SUB_NN_N;
            MUL_ND_N Mul_ND_N = UI.FindModule(typeof(MUL_ND_N)) as MUL_ND_N;

            Natural num1 = data[0] as Natural;
            Natural digit = data[2] as Natural;

            Natural num2 = Mul_ND_N.Calculate(data[1], digit) as Natural;

            string compare2to1 = UI.FindModule(typeof(COM_NN_D)).Calculate(num2, num1).GetString();

            if (compare2to1 == Messages.bigger)
                throw new UserException("Вы пытаетесь вычесть " + num2.GetString() + " из " + num1.GetString() + ", что дает отрицательный результат");

            return Sub_NN_N.Calculate(num1, num2);
        }
    }
}
