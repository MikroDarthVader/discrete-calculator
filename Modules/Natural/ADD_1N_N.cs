﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-3
    /// (Добавление 1 к натуральному числу)
    /// </summary>
    public class ADD_1N_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Добавление 1 к натуральному числу"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Число, к которому будет прибавлена единица" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num = (data[0] as Natural).Clone();

            int i;
            for (i = 0; i < num.Length && num[i] == 9; i++)
            {
                num[i] = 0;
            }

            if (num.Length == i)
                num.Insert(num.Length, 1);
            else
                num[i]++;

            return num;
        }
    }
}
