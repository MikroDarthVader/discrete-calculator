﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-5
    /// (Вычитание из первого большего натурального числа второго меньшего или равного)
    /// </summary>
    public class SUB_NN_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Вычитание из большего натурального числа меньшего или равного"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Уменьшаемое число", "Вычитаемое число" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = (data[0] as Natural).Clone();
            Natural num2 = data[1] as Natural;
            string compare2to1 = UI.FindModule(typeof(COM_NN_D)).Calculate(num2, num1).GetString();

            if (compare2to1 == Messages.bigger)
            {
                Natural temp = num2;
                num2 = num1;
                num1 = temp;
            }
            
            for (int i = 0; i < num1.Length; i++)
            {
                if (i < num2.Length)
                {
                    if (num1[i] < num2[i])
                    {
                        num1[i] -= (sbyte)(num2[i] - 10);
                        num1[i + 1] -= 1;
                    }
                    else
                    {
                        num1[i] -= num2[i];
                    }
                }
                else
                {
                    if (num1[i] == -1)
                    {
                        num1[i] = 9;
                        num1[i + 1] -= 1;
                    }
                    else
                        break;
                }
            }

            for (int i = num1.Length - 1; num1[i] == 0 && num1.Length != 1; i--)
                num1.Remove(i);

            return num1;
        }
    }
}