﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-12
    /// (Остаток от деления натуральных чисел)
    /// </summary>
    public class MOD_NN_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Остаток от деления натуральных чисел"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Делимое", "Делитель" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = data[0] as Natural;
            Natural num2 = data[1] as Natural;

            SUB_NN_N Sub_NN_N = UI.FindModule(typeof(SUB_NN_N)) as SUB_NN_N;
            DIV_NN_N Div_NN_N = UI.FindModule(typeof(DIV_NN_N)) as DIV_NN_N;
            MUL_NN_N Mul_NN_N = UI.FindModule(typeof(MUL_NN_N)) as MUL_NN_N;
            NZER_N_B Nzer_N_B = UI.FindModule(typeof(NZER_N_B)) as NZER_N_B;

            string compare2to1 = UI.FindModule(typeof(COM_NN_D)).Calculate(num2, num1).GetString();
            
            if (Nzer_N_B.Calculate(num2).GetString() == Messages.yes)
                throw new UserException("Деление на ноль не определенно");

            if (compare2to1 == Messages.bigger)
                return num1.Clone();
            else if (compare2to1 == Messages.equal)
                return new Natural(0);
            else
                return Sub_NN_N.Calculate(num1, (Mul_NN_N.Calculate((Div_NN_N.Calculate(num1, num2)), num2)));
        }
    }
}