﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-11
    /// (Частное от деления натуральных чисел с остатком)
    /// </summary>
    public class DIV_NN_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Частное от деления натуральных чисел с остатком"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Делимое", "Делитель" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = data[0] as Natural;
            Natural num2 = data[1] as Natural;
            Natural result = new Natural(0);
            Natural num1_new = (data[0] as Natural).Clone();

            ADD_NN_N Add_NN_N = UI.FindModule(typeof(ADD_NN_N)) as ADD_NN_N;
            SUB_NN_N Sub_NN_N = UI.FindModule(typeof(SUB_NN_N)) as SUB_NN_N;
            DIV_NN_Dk Div_NN_Dk = UI.FindModule(typeof(DIV_NN_Dk)) as DIV_NN_Dk;
            MUL_NN_N Mul_NN_N = UI.FindModule(typeof(MUL_NN_N)) as MUL_NN_N;
            COM_NN_D Com_NN_D = UI.FindModule(typeof(COM_NN_D)) as COM_NN_D;
            NZER_N_B Nzer_N_B = UI.FindModule(typeof(NZER_N_B)) as NZER_N_B;

            if (Nzer_N_B.Calculate(num2).GetString() == Messages.yes)
                throw new UserException("Деление на ноль не определенно");

            while (Com_NN_D.Calculate(num2, num1_new).GetString() == Messages.less || Com_NN_D.Calculate(num2, num1_new).GetString() == Messages.equal)
            {
                result = Add_NN_N.Calculate(result, Div_NN_Dk.Calculate(num1_new, num2) as Natural) as Natural;
                num1_new = Sub_NN_N.Calculate(num1, Mul_NN_N.Calculate(result, num2) as Natural) as Natural;
            }

            return result;
        }
    }
}