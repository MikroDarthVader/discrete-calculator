﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-14
    /// (НОК натуральных чисел)
    /// </summary>
    public class LCM_NN_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "НОК натуральных чисел"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Первое число", "Второе число" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = data[0] as Natural;
            Natural num2 = data[1] as Natural;
            Natural result;

            GCF_NN_N Gcf_NN_N = UI.FindModule(typeof(GCF_NN_N)) as GCF_NN_N;
            DIV_NN_N Div_NN_N = UI.FindModule(typeof(DIV_NN_N)) as DIV_NN_N;
            MUL_NN_N Mul_NN_N = UI.FindModule(typeof(MUL_NN_N)) as MUL_NN_N;
            NZER_N_B Nzer_N_B = UI.FindModule(typeof(NZER_N_B)) as NZER_N_B;

            if (Nzer_N_B.Calculate(num1).GetString() == Messages.yes || Nzer_N_B.Calculate(num2).GetString() == Messages.yes)
                return new Natural(0);

            return result = Div_NN_N.Calculate(Mul_NN_N.Calculate(num1, num2), Gcf_NN_N.Calculate(num1, num2)) as Natural;
        }
    }
}