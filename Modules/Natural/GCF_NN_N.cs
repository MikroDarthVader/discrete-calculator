﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Бурков Максим, Михайлов Никита, Коровин Михаил; Module N-13
    /// (НОД натуральных чисел)
    /// </summary>
    public class GCF_NN_N : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "НОД натуральных чисел"; } } // name in drop-down tree

        public moduleType Type { get { return moduleType.natural; } }

        public string[] Hints { get; } = new string[] { "Первое число", "Второе число" };

        public Type[] ReceiveType { get; } = new Type[] { typeof(Natural), typeof(Natural) };

        public Type ReturnType { get { return typeof(Natural); } }

        public IData Calculate(params IData[] data)
        {
            Natural num1 = (data[0] as Natural).Clone();
            Natural num2 = (data[1] as Natural).Clone();
            Natural tempMod;

            MOD_NN_N Mod_NN_N = UI.FindModule(typeof(MOD_NN_N)) as MOD_NN_N;
            NZER_N_B Nzer_N_B = UI.FindModule(typeof(NZER_N_B)) as NZER_N_B;

            string compare2to1 = UI.FindModule(typeof(COM_NN_D)).Calculate(num2, num1).GetString();

            if (compare2to1 == Messages.equal)
            {
                return num1;
            }

            if (compare2to1 == Messages.bigger)
            {
                Natural temp = num1;
                num1 = num2;
                num2 = temp;
            }

            while (Nzer_N_B.Calculate(num2).GetString() == Messages.no)
            {
                tempMod = Mod_NN_N.Calculate(num1, num2) as Natural;
                num1 = num2;
                num2 = tempMod;
            }

            return num1;
        }
    }
}
