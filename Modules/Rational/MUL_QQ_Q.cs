﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Криворучко Михаил, Михайлов Никита; Module Q-7
    /// (Умножение дробей)
    /// </summary>
    class MUL_QQ_Q : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Умножение дробей"; } }

        public moduleType Type { get { return moduleType.rational; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational), typeof(Rational) };

        public Type ReturnType { get { return typeof(Rational); } }

        public string[] Hints { get; } = new string[] { "1 рациональное", "2 рациональное" };

        public IData Calculate(params IData[] data)
        {
            MUL_ZZ_Z Mul_ZZ_Z = UI.FindModule(typeof(MUL_ZZ_Z)) as MUL_ZZ_Z;    // Используемые модули: Умножение целых; Умножение натуральных; Сокращение дроби
            MUL_NN_N Mul_NN_N = UI.FindModule(typeof(MUL_NN_N)) as MUL_NN_N;
            RED_Q_Q Red_Q_Q = UI.FindModule(typeof(RED_Q_Q)) as RED_Q_Q;

            Rational result = (data[0] as Rational).Clone();
            Rational num = data[1] as Rational;

            result.numerator = result.numerator * num.numerator as Integer;    // Умножение числитителей (MUL_ZZ_Z)
            result.denominator = result.denominator * num.denominator as Natural;   // Умножение знаменателей (MUL_NN_N)

            result = Red_Q_Q.Calculate(result) as Rational;     // Сокращение получившейся дроби (RED_Q_Q)

            return result;
        }
    }
}