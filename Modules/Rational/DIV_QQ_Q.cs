﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Криворучко Михаил, Михайлов Никита; Module Q-8
    /// (Деление левой дроби на правую)
    /// </summary>
    class DIV_QQ_Q : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Деление левой дроби на правую"; } }

        public moduleType Type { get { return moduleType.rational; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational), typeof(Rational) };

        public Type ReturnType { get { return typeof(Rational); } }

        public string[] Hints { get; } = new string[] { "1 рациональное", "2 рациональное" };

        public IData Calculate(params IData[] data)
        {
            MUL_NN_N Mul_NN_N = UI.FindModule(typeof(MUL_NN_N)) as MUL_NN_N;  // Используемые модули: Умножение натуральных; Сокращение дроби; Проверка на ноль
            RED_Q_Q Red_Q_Q = UI.FindModule(typeof(RED_Q_Q)) as RED_Q_Q;
            NZER_N_B Nzer_N_B = UI.FindModule(typeof(NZER_N_B)) as NZER_N_B;


            Rational num1 = (data[0] as Rational).Clone();
            Rational num2 = data[1] as Rational;
            Rational result = new Rational();

            if (Nzer_N_B.Calculate(num2.numerator.number).GetString() == Messages.yes)      // Если числитель второй дроби == 0, то деление невозможно, т.к. он оказывается в знаменателе результата (NZER_N_B)
                throw new UserException("Деление на ноль не определенно");

            result.numerator.sign = num1.numerator.sign == num2.numerator.sign ? signature.plus : signature.minus;      // Если знаки дробей совпадают, то в ответе "+", если различаются, то "-"    
            result.numerator.number = num1.numerator.number * num2.denominator as Natural;     //  Произведение числителя первой и знаменателя второй дроби - числитель (MUL_NN_N)
            result.denominator = num1.denominator * num2.numerator.number as Natural;       //  Произведение знаменателя первой и числителя второй дроби - знаменатель (MUL_NN_N)

            result = Red_Q_Q.Calculate(result) as Rational;     // Сокращение получившейся дроби (RED_Q_Q)

            return result;
        }
    }
}