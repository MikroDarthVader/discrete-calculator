﻿using Guinea.Environment;
using System;

namespace Guinea.Modules
{
    /// <summary>
    /// Криворучко Михаил, Михайлов Никита; Module Q-4
    /// (Преобразование дробного в целое)
    /// </summary>
    class TRANS_Q_Z : IModule
    {
        public bool VisibleInTree { get; } = false;

        public string PrintableName { get { return "Преобразование дробного в целое"; } }

        public moduleType Type { get { return moduleType.rational; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational) };

        public Type ReturnType { get { return typeof(Integer); } }

        public string[] Hints { get; } = new string[] { "Дробь" };

        public IData Calculate(params IData[] data)
        {
            RED_Q_Q Red_Q_Q = UI.FindModule(typeof(RED_Q_Q)) as RED_Q_Q;    // Используемые модули: Сокращение дроби; Сравнение натуральных
            COM_NN_D Com_NN_D = UI.FindModule(typeof(COM_NN_D)) as COM_NN_D;

            Rational num1 = Red_Q_Q.Calculate(data[0]) as Rational;      // Сокращение дроби

            if (Com_NN_D.Calculate(num1.denominator, new Natural(1)).GetString() == Messages.equal)     // Сравнение знаменателя с 1
                return num1.numerator;      // Если равен, то число целое
            else
                throw new UserException("Невозможно преобразовать дробь " + num1.numerator.GetString() + "/" + num1.denominator.GetString() + " в целое");    // Иначе - нельзя преобразовать в целое
        }
    }
}