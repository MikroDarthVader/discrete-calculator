using Guinea.Environment;
using System;

namespace Guinea.Modules
{
    /// <summary>
    /// Криворучко Михаил, Михайлов Никита; Module Q-5
    /// (Сложение дробей)
    /// </summary>
    class ADD_QQ_Q : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Сложение дробей"; } }

        public moduleType Type { get { return moduleType.rational; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational), typeof(Rational) };

        public Type ReturnType { get { return typeof(Rational); } }

        public string[] Hints { get; } = new string[] { "1 рациональное", "2 рациональное" };

        public IData Calculate(params IData[] data)
        {
            ADD_ZZ_Z Add_ZZ_Z = UI.FindModule(typeof(ADD_ZZ_Z)) as ADD_ZZ_Z; // Используемые модули: Сложение целых; Умножение целых; Умножение натуральных; Сокращение дроби
            MUL_ZZ_Z Mul_ZZ_Z = UI.FindModule(typeof(MUL_ZZ_Z)) as MUL_ZZ_Z;
            MUL_NN_N Mul_NN_N = UI.FindModule(typeof(MUL_NN_N)) as MUL_NN_N;
            RED_Q_Q Red_Q_Q = UI.FindModule(typeof(RED_Q_Q)) as RED_Q_Q;

            Rational num1 = data[0] as Rational;
            Rational num2 = data[1] as Rational;

            if (num1.Equals(Natural.zero))
                return num2;
            else if (num2.Equals(Natural.zero))
                return num1;

            Integer denominator1 = new Integer(signature.plus, num1.denominator);
            Integer denominator2 = new Integer(signature.plus, num2.denominator);

            Integer numerator = (num1.numerator * denominator2) + (num2.numerator * denominator1) as Integer;    // Сложение числителей, домноженных на знаменатели противоположной дроби.
            Natural denominator = num1.denominator * num2.denominator as Natural;   // Умножение знаменателей

            return Red_Q_Q.Calculate(new Rational(numerator, denominator));     // Сокращение получивейся дроби (RED_Q_Q)
        }
    }
}