﻿using Guinea.Environment;
using System;

namespace Guinea.Modules
{
    /// <summary>
    /// Криворучко Михаил, Михайлов Никита; Module Q-3
    /// (Преобразование целого в дробное)
    /// </summary>
    class TRANS_Z_Q : IModule
    {
        public bool VisibleInTree { get; } = false;

        public string PrintableName { get { return "Преобразование целого в дробное"; } }

        public moduleType Type { get { return moduleType.rational; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Integer) };

        public Type ReturnType { get { return typeof(Rational); } }

        public string[] Hints { get; } = new string[] { "Целое число" };

        public IData Calculate(params IData[] data)
        {
            return new Rational((data[0] as Integer).Clone(), new Natural(1));  // Добавление еденицы в знаменатель новой дроби
        }
    }
}
