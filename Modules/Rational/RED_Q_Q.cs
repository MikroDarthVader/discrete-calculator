﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Криворучко Михаил, Михайлов Никита; Module Q-1
    /// (Сокращение дроби)
    /// </summary>
    class RED_Q_Q : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Сокращение дроби"; } }

        public moduleType Type { get { return moduleType.rational; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational) };

        public Type ReturnType { get { return typeof(Rational); } }

        public string[] Hints { get; } = new string[] { "Сократимая дробь" };

        public IData Calculate(params IData[] data)
        {
            ABS_Z_N Abs_Z_N = UI.FindModule(typeof(ABS_Z_N)) as ABS_Z_N;        // Используемые модули: Модуль целого; НОД натуральных; Деление целых; Деление натуральных
            GCF_NN_N Gcf_NN_N = UI.FindModule(typeof(GCF_NN_N)) as GCF_NN_N;
            DIV_ZZ_Z Div_ZZ_Z = UI.FindModule(typeof(DIV_ZZ_Z)) as DIV_ZZ_Z;
            DIV_NN_N Div_NN_N = UI.FindModule(typeof(DIV_NN_N)) as DIV_NN_N;

            Rational result = (data[0] as Rational).Clone();

            Natural nod = Gcf_NN_N.Calculate(Abs_Z_N.Calculate(result.numerator), result.denominator) as Natural;   // НОД модуля числителя (ABS_Z_N) и значенателя (GCF_NN_N)
            result.numerator = Div_ZZ_Z.Calculate(result.numerator, nod) as Integer;    // Деление числателя на НОД (DIV_ZZ_Z)
            result.denominator = result.denominator / nod as Natural;    // Деление знаменателя на НОД (DIV_NN_N)

            return result;
        }
    }
}