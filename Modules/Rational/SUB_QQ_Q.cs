﻿using Guinea.Environment;
using System;

namespace Guinea.Modules
{
    /// <summary>
    /// Криворучко Михаил, Михайлов Никита; Module Q-6
    /// (Вычитание из левой дроби правой)
    /// </summary>
    class SUB_QQ_Q : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Вычитание из левой дроби правой"; } }

        public moduleType Type { get { return moduleType.rational; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational), typeof(Rational) };

        public Type ReturnType { get { return typeof(Rational); } }

        public string[] Hints { get; } = new string[] { "1 дробь", "2 дробь" };

        public IData Calculate(params IData[] data)
        {
            ADD_QQ_Q Add_QQ_Q = UI.FindModule(typeof(ADD_QQ_Q)) as ADD_QQ_Q;    // Используемые модули: Сложение дробей; Умножение целого на (-1)
            MUL_ZM_Z Mul_ZM_Z = UI.FindModule(typeof(MUL_ZM_Z)) as MUL_ZM_Z;

            Rational num = (data[1] as Rational).Clone();

            num.numerator = Mul_ZM_Z.Calculate(num.numerator) as Integer;   // Умножение числитителя второй дроби на (-1) (MUL_ZM_Z)

            return Add_QQ_Q.Calculate(data[0], num);    // Сложение дробей (ADD_QQ_Q)
        }
    }
}