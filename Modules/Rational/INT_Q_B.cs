﻿using System;
using Guinea.Environment;

namespace Guinea.Modules
{
    /// <summary>
    /// Криворучко Михаил, Михайлов Никита; Module Q-2
    /// (Проверка на целое)
    /// </summary>
    class INT_Q_B : IModule
    {
        public bool VisibleInTree { get; } = true;

        public string PrintableName { get { return "Проверка на целое"; } }

        public moduleType Type { get { return moduleType.rational; } }

        public Type[] ReceiveType { get; } = new Type[] { typeof(Rational) };

        public Type ReturnType { get { return typeof(MessageData); } }

        public string[] Hints { get; } = new string[] { "Рациональное число" };

        public IData Calculate(params IData[] data)
        {
            ABS_Z_N Abs_Z_N = UI.FindModule(typeof(ABS_Z_N)) as ABS_Z_N;    // Используемые модули: Модуль целого; НОД натуральных; Сравнение натуральных
            GCF_NN_N Gcf_NN_N = UI.FindModule(typeof(GCF_NN_N)) as GCF_NN_N;
            COM_NN_D Com_NN_D = UI.FindModule(typeof(COM_NN_D)) as COM_NN_D;

            Rational num1 = data[0] as Rational;

            Natural nod = Gcf_NN_N.Calculate(Abs_Z_N.Calculate(num1.numerator), num1.denominator) as Natural;       // Ищем наибольший общий делитель числителя и знаменателя (GCF_NN_N)

            if (Com_NN_D.Calculate(nod, num1.denominator).GetString() == Messages.equal)    // Если НОД равен знаменателю => дробь можно сократить на НОД и получить INT/1 (COM_NN_D)
                return new MessageData(Messages.yes);
            else                                                                            // Если не равен, то дробь несократима
                return new MessageData(Messages.no);
        }
    }
}
