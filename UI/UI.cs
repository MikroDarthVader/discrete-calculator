﻿using System;
using System.Collections.Generic;
using System.Linq;
using Guinea.Environment;
using Guinea.Modules;

namespace Guinea
{
    public static class UI
    {
        public static string ModulesRegex;
        public static string DatasRegex = @"((((^|\n|\r|;|\(|=|\+|-)\s*)|[+-](x|\d))((\d+(/\d+)?(\*x(\^\d+)?)?)|(x(\^\d+)?)))*";
        public static Dictionary<int, IModule> Modules = new Dictionary<int, IModule>();
        public static Dictionary<int, IData> Datas = new Dictionary<int, IData>();
        public static Dictionary<Type, string> PrintableDatas = new Dictionary<Type, string>() { { typeof(Natural), "натуральное" }, { typeof(Integer), "целое" }, { typeof(Rational), "рациональное" }, { typeof(Polynom), "многочлен" }, { typeof(MessageData), "сообщение" } };

        public static IModule FindModule(Type type)
        {
            return Modules[type.GetHashCode()];
        }

        public static IData FindData(Type type)
        {
            return Datas[type.GetHashCode()];
        }

        public static string TryParse(string str)
        {
            return TryParseAsData(str).GetString();
        }

        public static IData TryParseAsData(string str, Type type = null)
        {
            IData result = null;
            str = str.Trim(' ');

            if (type == null)
            {
                foreach (IData data in Datas.Values)
                {
                    result = data.TryParse(str);
                    if (result != null)
                        return result;
                }
            }
            else
            {
                foreach (IData data in Datas.Values)
                {
                    try { result = data.TryParse(str); }
                    catch { continue; }
                    if (result != null)
                    {
                        result = TryTransformTo(result, type);
                        if (result != null)
                            return result;
                        else throw new WrongParameterTypeException();
                    }
                }
            }

            result = TryParseAsModule(str, type);

            return result;
        }

        public static IData TryParseAsModule(string str, Type type = null)
        {
            if (str == null || str.Length == 0)
                throw new CanNotParseException();

            List<string> elements;
            Type moduleType = null;

            try { elements = new List<string>(SplitByArguments(str)); } catch { throw new ForgotBracketsException(); }
            if (str[str.Length - 1] != ')')
                throw new CanNotParseException();

            elements[0] = elements[0].Trim(' ');
            if (ModulesRegex.Contains(elements[0]))
                moduleType = Type.GetType("Guinea.Modules." + elements[0]);
            if (moduleType == null)
                throw new WrongModuleNameException();

            elements.RemoveAt(0);
            IModule module = FindModule(moduleType);

            if (elements.Count != module.ReceiveType.Length)
                throw new WrongParametersNumbereException();

            List<IData> parameters = new List<IData>();

            for (int i = 0; i < elements.Count; i++)
            {
                IData parameter = TryParseAsData(elements[i], module.ReceiveType[i]);
                parameters.Add(parameter);
            }

            return TryTransformTo(module.Calculate(parameters.ToArray()), type);
        }

        public static IData TryTransformTo(IData from, Type to)
        {
            if (to == null)
                return from;

            int fromDataLevel;

            try { fromDataLevel = GetDataLevel(from.GetType()); }
            catch { throw new WrongParameterTypeException(); }

            int toDataLevel = GetDataLevel(to);

            if (toDataLevel - fromDataLevel > 0)
            {
                switch (fromDataLevel)
                {
                    case 0: return TryTransformTo(FindModule(typeof(TRANS_N_Z)).Calculate(from), to);
                    case 1: return TryTransformTo(FindModule(typeof(TRANS_Z_Q)).Calculate(from), to);
                    case 2: return TryTransformTo(FindModule(typeof(TRANS_Q_P)).Calculate(from), to);
                }
            }

            if (toDataLevel - fromDataLevel < 0)
            {
                switch (fromDataLevel)
                {
                    case 3: return TryTransformTo(FindModule(typeof(TRANS_P_Q)).Calculate(from), to);
                    case 2: return TryTransformTo(FindModule(typeof(TRANS_Q_Z)).Calculate(from), to);
                    case 1: return TryTransformTo(FindModule(typeof(TRANS_Z_N)).Calculate(from), to);
                }
            }

            if (toDataLevel - fromDataLevel == 0)
                return from;

            return null;
        }

        public static int GetDataLevel(Type dataType)
        {
            Type[] types = new Type[] { typeof(Natural), typeof(Integer), typeof(Rational), typeof(Polynom)};
            for (int i = 0; i < types.Length; i++)
                if (dataType == types[i]) return i;
            throw new Exception("Trying to get level of non numeric data");
        }

        private static List<string> SplitByArguments(string str)
        {
            List<string> result = new List<string> { "" };

            int bracketsDelta = 0;

            foreach (char symbol in str)
            {
                if (symbol == '(')
                    bracketsDelta++;
                if (symbol == ')')
                    bracketsDelta--;

                if ((result.Count == 1 || symbol == ';') && bracketsDelta == 1)
                {
                    result.Add("");
                }
                else if (bracketsDelta != 0 || symbol != ')')
                    result[result.Count - 1] += symbol;
            }

            result.RemoveAll(x => x == "");

            if (bracketsDelta != 0)
                return null;
            else
                return result;
        }
    }
}