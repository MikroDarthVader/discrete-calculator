﻿using System;

namespace Guinea
{
    public class UserException : Exception
    {
        public UserException() : base() { }

        public UserException(string message) : base(message) { }
    }

    public class CanNotParseException : UserException
    {
        public CanNotParseException() : base("Can't Parse") { }
    }

    public class ForgotBracketsException : UserException
    {
        public ForgotBracketsException() : base("Forgot Brackets") { }
    }

    public class WrongModuleNameException : UserException
    {
        public WrongModuleNameException() : base("Wrong Module Name") { }
    }

    public class WrongParametersNumbereException : UserException
    {
        public WrongParametersNumbereException() : base("Wrong Parameters Number") { }
    }

    public class WrongParameterTypeException : UserException
    {
        public WrongParameterTypeException() : base("Wrong Parameter Type") { }
    }
}
